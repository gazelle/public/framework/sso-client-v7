package net.ihe.gazelle.ssov7.authn.interlay.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static net.ihe.gazelle.ssov7.authn.interlay.dto.TestKeycloakClientDTO.NO_PROTOCOL;
import static org.junit.Assert.*;

public class KeycloakClientDTOTest {

    @Test
    public void keycloakClientDTOTest() {
        TestKeycloakClientDTO testKeycloakClientDTO = new TestKeycloakClientDTO();
        String testAbstract = "testAbstract";
        String clientAuthenticatorTypeAbstract = "clientAuthenticatorTypeAbstract";
        List<String> clientScopes = Arrays.asList("scope1", "scope2");
        List<String> optionalClientScopes = Arrays.asList("optionalScope1", "optionalScope2");
        String name = "abstractName";
        String baseUrl = "https://base.url";
        List<String> protocolMappers = Arrays.asList("protocolMapper1", "protocolMapper2");
        List<String> redirectUris = Arrays.asList("https://redirectUri1.fake", "https://redirectUri2.fake");

        testKeycloakClientDTO.setClientId(testAbstract);
        testKeycloakClientDTO.setClientAuthenticatorType(clientAuthenticatorTypeAbstract);
        testKeycloakClientDTO.setDefaultClientScopes(clientScopes);
        testKeycloakClientDTO.setOptionalClientScopes(optionalClientScopes);
        testKeycloakClientDTO.setName(name);
        testKeycloakClientDTO.setBaseUrl(baseUrl);

        testKeycloakClientDTO.setProtocolMappers(protocolMappers);
        testKeycloakClientDTO.setRedirectUris(redirectUris);

        assertEquals(NO_PROTOCOL, testKeycloakClientDTO.getProtocol());
        assertEquals(testAbstract, testKeycloakClientDTO.getClientId());
        assertEquals(clientAuthenticatorTypeAbstract, testKeycloakClientDTO.getClientAuthenticatorType());
        assertEquals(clientScopes, testKeycloakClientDTO.getDefaultClientScopes());
        assertEquals(optionalClientScopes, testKeycloakClientDTO.getOptionalClientScopes());
        assertEquals(name, testKeycloakClientDTO.getName());
        assertEquals(baseUrl, testKeycloakClientDTO.getBaseUrl());
        assertEquals(protocolMappers, testKeycloakClientDTO.getProtocolMappers());
        assertEquals(redirectUris, testKeycloakClientDTO.getRedirectUris());

    }

    @Test
    public void equalsAndHashCodeTest() {
        EqualsVerifier.forClass(TestKeycloakClientDTO.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
    }

}