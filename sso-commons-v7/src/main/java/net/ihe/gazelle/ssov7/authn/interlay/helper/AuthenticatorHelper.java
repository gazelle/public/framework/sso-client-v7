package net.ihe.gazelle.ssov7.authn.interlay.helper;

import org.jboss.seam.web.ServletContexts;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class AuthenticatorHelper {

    public HttpServletRequest getHttpServletRequestFromFaceContext() {
        FacesContext facesContext = FacesContext.getCurrentInstance();
        HttpServletRequest request = null;
        if (facesContext != null) {
            ExternalContext externalContext = facesContext.getExternalContext();
            if (externalContext != null) {
                request = (HttpServletRequest) externalContext.getRequest();
            }
        }
        return request;
    }

    public HttpServletRequest getHttpServletRequestFromServletContext() {
        ServletContexts contexts = ServletContexts.getInstance();
        HttpServletRequest request = null;
        if (contexts != null) {
            request = contexts.getRequest();
            if (request != null) {
                HttpSession session = request.getSession();
                if (session != null) {
                    return request;
                }
            }
        }
        return request;
    }
}
