package net.ihe.gazelle.ssov7.gum.client.interlay.filter;

import net.ihe.gazelle.common.filter.Filter;
import net.ihe.gazelle.common.filter.criterion.ValueFormatter;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import org.jboss.seam.Component;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.NoSuchElementException;

public class UserValueFormatter implements ValueFormatter, Serializable {

   private static final long serialVersionUID = 4845939594237336028L;
   private final Filter<?> filter;
   private final String keyword;
   private transient UserService userService;

   public UserValueFormatter(Filter<?> filter, String keyword) {
      this.filter = filter;
      this.keyword = keyword;
      this.userService = (UserService) Component.getInstance("gumUserService");
   }


   @Override
   public String format(Object o) {
      return formatLabel(o);
   }

   @Override
   public String formatLabel(Object o) {
      String selectableLabel = "";
      if (o instanceof String && !((String) o).isEmpty()) {
         try {
            selectableLabel = userService.getUserById((String) o).getFirstNameAndLastName();
            if (filter.isCountEnabled()) {
               return "(" + formatCount(o) + ") " + selectableLabel;
            }
         } catch (NoSuchElementException e) {
            // When value returned is null, then it is not displayed in filter.
            return null;
         }
      }
      return selectableLabel;
   }

   @Override
   public String formatCount(Object o) {
      if (o != null) {
         List<Integer> counts = filter.getPossibleValuesCount(keyword);
         if (counts != null) {
            List<Object> values = filter.getPossibleValues(keyword);
            if (values != null) {
               int index = values.indexOf(o);
               if (index != -1) {
                  Integer count = counts.get(index);
                  return Integer.toString(count);
               }
            }
         }
      }
      return "0";
   }


   /**
    * Reload userService in case of deserialization.
    *
    * @param in inputstream of the UserValueFormatter.
    *
    * @throws IOException
    * @throws ClassNotFoundException
    * @see Serializable
    */
   private void readObject(java.io.ObjectInputStream in) throws IOException, ClassNotFoundException {
      in.defaultReadObject();
      this.userService = (UserService) Component.getInstance("gumUserService");
   }
}
