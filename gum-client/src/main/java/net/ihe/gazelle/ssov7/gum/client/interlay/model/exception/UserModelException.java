package net.ihe.gazelle.ssov7.gum.client.interlay.model.exception;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 18/09/2023
 */
public class UserModelException extends RuntimeException{

    private static final long serialVersionUID = -4084836449754802356L;

    public UserModelException() {
    }

    public UserModelException(String message) {
        super(message);
    }

    public UserModelException(String message, Throwable cause) {
        super(message, cause);
    }

    public UserModelException(Throwable cause) {
        super(cause);
    }
}
