package net.ihe.gazelle.ssov7.gum.client.interlay.cache;

import com.google.common.cache.CacheBuilder;
import com.google.common.cache.CacheLoader;
import com.google.common.cache.LoadingCache;
import com.google.common.util.concurrent.UncheckedExecutionException;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.ssov7.gum.client.interlay.model.entity.UserSearchResult;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeUnit;

/**
 * @author Claude LUSSEAU
 */
public class SSOGumUserClientCache implements UserService {

   private static final Logger LOGGER = LoggerFactory.getLogger(SSOGumUserClientCache.class);
   public static final int EXPIRATION;
   public static final int DEFAULT_EXPIRATION_MINUTES = 15;

   private static final String CACHE_EXPIRATION_MINUTES_ENV = "GZL_USER_CACHE_EXPIRATION_MINUTES";
   private static final String UNKNOWN_USER = "Unknown user";

   static {
      String exp = System.getenv(CACHE_EXPIRATION_MINUTES_ENV);
      EXPIRATION = exp != null ? Integer.parseInt(exp) : DEFAULT_EXPIRATION_MINUTES;
   }
   private final UserService userService;

   private final LoadingCache<String, User> userCache = CacheBuilder.newBuilder().maximumSize(2000)
         .expireAfterWrite(EXPIRATION, TimeUnit.MINUTES).expireAfterAccess(EXPIRATION, TimeUnit.MINUTES).build(
               new CacheLoader<String, User>() {
                  @Override
                  public User load(String userId) throws Exception {
                     if (Boolean.TRUE.equals(notFoundCache.get(userId))) {
                        throw new NoSuchElementException("User not found " + userId);
                     } else {
                        return userService.getUserById(userId);
                     }
                  }
               }
         );
   private final LoadingCache<String, Boolean> notFoundCache = CacheBuilder.newBuilder().maximumSize(500)
         .expireAfterWrite(EXPIRATION, TimeUnit.MINUTES).expireAfterAccess(EXPIRATION, TimeUnit.MINUTES).build(
               new CacheLoader<String, Boolean>() {
                  @Override
                  public Boolean load(String userId) throws Exception {
                     // By default set the not-found cache to false. That way, when userNotFound.get(id) return
                      // false, it means that it must be fetched from GUM.
                     return false;
                  }
               });

   public SSOGumUserClientCache(UserService userService) {
      this.userService = userService;
   }


   @Override
   public UserSearchResult searchAndFilter(UserSearchParams userSearchParams) {
      LOGGER.trace("searchAndFilter");
      return userService.searchAndFilter(userSearchParams);
   }

   @Override
   public List<User> searchNoLimit(UserSearchParams userSearchParams) {
      LOGGER.trace("searchNoLimit");
      return userService.searchNoLimit(userSearchParams);
   }

   @Override
   public Map<String, Long> getValueCount(String propertyName, String search, String organizationId, String role,
                                          Boolean activated) {
      return userService.getValueCount(propertyName, search, organizationId, role, activated);
   }

   @Override
   public User getUserById(final String userId) {
      LOGGER.trace("getUserById");
      try {
         return userCache.get(userId);
      } catch (UncheckedExecutionException e) {
         if (e.getCause() instanceof NoSuchElementException) {
            notFoundCache.put(userId, true);
            throw (NoSuchElementException) e.getCause();
         }
         throw e;
      } catch (ExecutionException e) {
         throw new UserServiceCacheException("Unexpected cache error.", e);
      }
   }


   @Override
   public void removeUserCache(String userId) {
      userCache.invalidate(userId);
   }

   @Override
   public String getUserDisplayNameWithoutException(String userId) {
      if(userId != null && !userId.isEmpty()) {
         try {
            return getUserById(userId).getFirstNameAndLastName();
         } catch (Exception e) {
            LOGGER.info("Unable to retrieve user {} : {} ", userId, e.getMessage());
            return UNKNOWN_USER;
         }
      } else {
         return UNKNOWN_USER;
      }
   }

    public static class UserServiceCacheException extends RuntimeException {
        private static final long serialVersionUID = 5134061898994154967L;

        public UserServiceCacheException(String message, Throwable cause) {
            super(message, cause);
        }
    }
}
