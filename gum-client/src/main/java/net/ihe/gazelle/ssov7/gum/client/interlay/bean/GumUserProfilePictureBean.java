package net.ihe.gazelle.ssov7.gum.client.interlay.bean;


import net.ihe.gazelle.preferences.PreferenceService;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserPreferencesService;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.Serializable;
import java.util.Map;


@AutoCreate
@Name(value = "gumUserProfilePictureBean")
@Scope(ScopeType.SESSION)
@Synchronized(timeout = 10000)
public class GumUserProfilePictureBean implements Serializable {

    private static final long serialVersionUID = 117057187474359348L;
    private static final String CONTENT_TYPE_IMAGE_JPEG = "image/jpeg";


    @In(value = "userPreferencesService")
    private transient UserPreferencesService userPreferencesService;

    public String getUserProfilePictureUri(String userId) {
        return getUserPhotoUri(userId, "normal");
    }

    public String getUserProfileThumbnailUri(String userId) {
        return getUserPhotoUri(userId, "thumbnail");
    }

    /**
     * This method is used to render a picture from the query parameters userId and format
     * userId is the id  of the user you want the picture
     * format is the format of the picture i.e. normal or thumbnail
     */
    public void getUserProfilePicture() {

        FacesContext facesContext = FacesContext.getCurrentInstance();
        ExternalContext externalContext = facesContext.getExternalContext();
        HttpServletResponse response = (HttpServletResponse) externalContext.getResponse();
        response.reset();
        response.setContentType(CONTENT_TYPE_IMAGE_JPEG);

        Map<String, String> params = externalContext.getRequestParameterMap();
        final String userId = params.get("userId");
        final String format = params.get("format");
        byte[] bytes = userPreferencesService.getUserProfilePicture(userId, format);
        try (ServletOutputStream outputStream = response.getOutputStream()) {
            outputStream.write(bytes);
            outputStream.flush();
            facesContext.responseComplete();
        } catch (IOException e) {
            throw new GumUserProfilePictureBeanException("Could not write image", e);
        }
    }

    private String getUserPhotoUri(String userId, String format) {
        String applicationUrl = PreferenceService.getString("application_url");
        if(!applicationUrl.endsWith("/"))
            applicationUrl += "/";
        return applicationUrl + "userPhoto.seam?userId=" + userId + "&format=" + format;
    }

    /**
     * Reload userPreferencesService in case of deserialization.
     *
     * @param inputStream InputStream of the UserValueFormatter.
     * @throws IOException
     * @throws ClassNotFoundException
     * @see Serializable
     */
    private void readObject(java.io.ObjectInputStream inputStream) throws IOException, ClassNotFoundException {
        inputStream.defaultReadObject();
        this.userPreferencesService = (UserPreferencesService) Component.getInstance("userPreferencesService");
    }
}
