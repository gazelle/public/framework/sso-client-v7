package net.ihe.gazelle.ssov7.gum.client.interlay.model.entity;

import net.ihe.gazelle.ssov7.gum.client.application.User;

import java.util.List;

public class UserSearchResult {

    private List<User> users;
    private Integer offset;
    private Integer limit;
    private Long count;

    public UserSearchResult(List<User> users, Integer offset, Integer limit, Long count) {
        this.users = users;
        this.offset = offset;
        this.limit = limit;
        this.count = count;
    }

    public UserSearchResult() {
    }

    public List<User> getUsers() {
        return users;
    }

    public void setUsers(List<User> users) {
        this.users = users;
    }

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getLimit() {
        return limit;
    }

    public void setLimit(Integer limit) {
        this.limit = limit;
    }

    public Long getCount() {
        return count;
    }

    public void setCount(Long count) {
        this.count = count;
    }

}
