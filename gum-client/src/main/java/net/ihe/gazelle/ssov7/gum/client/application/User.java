package net.ihe.gazelle.ssov7.gum.client.application;

import java.beans.Transient;
import java.io.Serializable;
import java.sql.Timestamp;
import java.util.*;

/**
 * @author Clément LAGORCE, Claude LUSSEAU
 * @company KEREVAL
 * @project gazelle-tm
 * @date 29/08/2023
 */
public class User implements Serializable, Comparable<User> {

    private static final long serialVersionUID = 4204330319573277802L;
    public static final ThreadLocal<String> remoteLogin = new ThreadLocal<>();
    private String id;
    private String firstName;
    private String lastName;
    private String email;
    private Set<String> groupIds = new HashSet<>();
    private String organizationId;
    private Boolean activated;
    private Timestamp lastLoginTimestamp;
    private Integer loginCounter;
    private Timestamp lastUpdateTimestamp;
    private Boolean delegated = false;
    private String externalId;
    private String idpId;

    public User() {
    }

    public User(String firstName, String lastName, String email, Set<String> groupIds, String organizationId, Boolean activated, Timestamp lastLoginTimestamp, Integer loginCounter, Timestamp lastUpdateTimestamp) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.groupIds = groupIds != null ? new HashSet<>(groupIds) : new HashSet<String>();
        this.organizationId = organizationId;
        this.activated = activated;
        this.externalId = null;
        this.idpId = null;
        this.lastLoginTimestamp = lastLoginTimestamp;
        this.loginCounter = loginCounter;
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public User(String id, String firstName, String lastName, String email, Set<String> groupIds, String organizationId, Boolean activated, Timestamp lastLoginTimestamp, Integer loginCounter, Timestamp lastUpdateTimestamp) {
        this(firstName, lastName, email, groupIds, organizationId, activated, lastLoginTimestamp, loginCounter, lastUpdateTimestamp);
        this.id = id;
        this.externalId = null;
        this.idpId = null;
    }

    public User(String firstName, String lastName, String email, Set<String> groupIds, String organizationId, Boolean activated, String externalId, String idpId, Timestamp lastLoginTimestamp, Integer loginCounter, Timestamp lastUpdateTimestamp) {
        this(firstName, lastName, email, groupIds, organizationId, activated, lastLoginTimestamp, loginCounter, lastUpdateTimestamp);
        this.externalId = externalId;
        this.idpId = idpId;
        updateDelegated();
    }

    public User(String id, String firstName, String lastName, String email, Set<String> groupIds, String organizationId, Boolean activated, String externalId, String idpId, Timestamp lastLoginTimestamp, Integer loginCounter, Timestamp lastUpdateTimestamp) {
        this(firstName, lastName, email, groupIds, organizationId, activated, externalId, idpId, lastLoginTimestamp, loginCounter, lastUpdateTimestamp);
        this.id = id;
    }

    public User(User copyUser) {
        this(copyUser.firstName,
                copyUser.lastName,
                copyUser.email,
                copyUser.groupIds,
                copyUser.organizationId,
                copyUser.activated,
                copyUser.externalId,
                copyUser.idpId,
                copyUser.lastLoginTimestamp,
                copyUser.loginCounter,
                copyUser.lastUpdateTimestamp);
        this.id = copyUser.getId();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void addGroupId(String groupId) {
        groupIds.add(groupId);
    }

    public Set<String> getGroupIds() {
        return new HashSet<>(groupIds);
    }

    public void setGroupIds(Set<String> groupIds) {
        this.groupIds = groupIds == null
                ? new HashSet<String>()
                : new HashSet<>(groupIds);
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
    }

    public Boolean getActivated() {
        return activated;
    }

    public void setActivated(Boolean activated) {
        this.activated = activated;
    }

    public Timestamp getLastLoginTimestamp() {
        return lastLoginTimestamp;
    }

    public void setLastLoginTimestamp(Timestamp lastLoginTimestamp) {
        this.lastLoginTimestamp = lastLoginTimestamp;
    }

    public Integer getLoginCounter() {
        return loginCounter;
    }

    public void setLoginCounter(Integer loginCounter) {
        this.loginCounter = loginCounter;
    }

    public Timestamp getLastUpdateTimestamp() {
        return lastUpdateTimestamp;
    }

    public void setLastUpdateTimestamp(Timestamp lastUpdateTimestamp) {
        this.lastUpdateTimestamp = lastUpdateTimestamp;
    }

    public boolean hasGroup(String group) {
        if (group == null) {
            return false;
        }
        return groupIds.contains(group);
    }

    public boolean hasRole(String role) {
        if (role == null)
            return false;

        switch (role) {
            case Role.USER:
                return true;
            case Role.VENDOR:
                for(String group : groupIds) {
                    if (group.startsWith(Group.GRP_ORGA_MEMBER)) return true;
                }
                return false;
            case Role.VENDOR_ADMIN:
                for(String group : groupIds) {
                    if (group.startsWith(Group.GRP_ORGA_ADMIN)) return true;
                }
                return false;
            case Role.ADMIN :
                return groupIds.contains(Group.GRP_GAZELLE_ADMIN);
            case Role.MONITOR:
                return groupIds.contains(Group.GRP_MONITOR);
            case Role.TESTING_SESSION_ADMIN:
                return groupIds.contains(Group.GRP_TESTING_SESSION_MANAGER);
            case Role.VENDOR_LATE_REGISTRATION:
                return groupIds.contains(Group.GRP_LATE_REGISTRATION);
            case Role.PROJECT_MANAGER:
                return groupIds.contains(Group.GRP_PROJECT_ADMIN);
            case Role.TESTS_EDITOR:
                return groupIds.contains(Group.GRP_TEST_DESIGNER);
            default:
                return false;
        }
    }

    public Boolean getDelegated() {
        return this.delegated;
    }

    public void setDelegated(Boolean delegated) {
        this.delegated = delegated;
    }

    private void updateDelegated() {
        this.delegated = externalId != null && idpId != null;
    }

    public String getExternalId() {
        return externalId;
    }

    public void setExternalId(String externalId) {
        this.externalId = externalId;
        updateDelegated();
    }

    public String getIdpId() {
        return idpId;
    }

    public void setIdpId(String idpId) {
        this.idpId = idpId;
        updateDelegated();
    }

    @Transient
    public String getFirstNameAndLastName() {
        return firstName + " " + lastName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        User user = (User) o;
        return Objects.equals(id, user.id)
                && Objects.equals(firstName, user.firstName)
                && Objects.equals(lastName, user.lastName)
                && Objects.equals(email, user.email)
                && Objects.equals(groupIds, user.groupIds)
                && Objects.equals(organizationId, user.organizationId)
                && Objects.equals(activated, user.activated)
                && Objects.equals(lastLoginTimestamp, user.lastLoginTimestamp)
                && Objects.equals(loginCounter, user.loginCounter)
                && Objects.equals(lastUpdateTimestamp, user.lastUpdateTimestamp)
                && Objects.equals(delegated, user.delegated)
                && Objects.equals(externalId, user.externalId)
                && Objects.equals(idpId, user.idpId);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, firstName, lastName, email, groupIds, organizationId, activated, lastLoginTimestamp, loginCounter, lastUpdateTimestamp, delegated, externalId, idpId);
    }

    @Override
    public String toString() {
        return "User{" + "id='" + id + '\'' +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", email='" + email + '\'' +
                ", groupIds=" + groupIds +
                ", organizationId='" + organizationId + '\'' +
                ", activated=" + activated +
                ", lastLoginTimestamp=" + lastLoginTimestamp +
                ", loginCounter=" + loginCounter +
                ", lastUpdateTimestamp=" + lastUpdateTimestamp +
                ", externalId=" + externalId +
                ", idpId=" + idpId +
                '}';
    }

    @Override
    public int compareTo(User o) {
        if (this.getFirstName() == null) {
            return -1;
        }
        if (o.getFirstName() == null) {
            return 1;
        }
        return this.getFirstName().compareToIgnoreCase(o.getFirstName());
    }

    public User get(String userId) {
        return new User(userId, firstName, lastName, email, groupIds, organizationId, activated, externalId, idpId, lastLoginTimestamp, loginCounter, lastUpdateTimestamp);
    }
}
