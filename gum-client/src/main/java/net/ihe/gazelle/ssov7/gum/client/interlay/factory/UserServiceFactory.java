package net.ihe.gazelle.ssov7.gum.client.interlay.factory;

import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.cache.SSOGumUserClientCache;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.SSOGumUserClient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;


@Name("userServiceFactory")
@Scope(ScopeType.APPLICATION)
@Install
@AutoCreate
public class UserServiceFactory {

    private static SSOGumUserClientCache userServiceCache ;

    @Factory(value = "gumUserService", autoCreate = true, scope = ScopeType.APPLICATION)
    public static UserService getUserService() {
        if(userServiceCache == null) {
            userServiceCache = new SSOGumUserClientCache(new SSOGumUserClient());
        }
        return userServiceCache;
    }
}
