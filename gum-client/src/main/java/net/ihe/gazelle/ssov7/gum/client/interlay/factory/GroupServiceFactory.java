package net.ihe.gazelle.ssov7.gum.client.interlay.factory;

import net.ihe.gazelle.ssov7.gum.client.application.service.GroupService;
import net.ihe.gazelle.ssov7.gum.client.interlay.cache.SSOGumGroupClientCache;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.SSOGumGroupsClient;
import org.jboss.seam.Component;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("groupServiceFactory")
@Scope(ScopeType.APPLICATION)
@Install
@AutoCreate
public class GroupServiceFactory {

    @Factory(value = "gumGroupService", autoCreate = true, scope = ScopeType.APPLICATION)
    public static GroupService getRoleService() {
        GroupService groupService = (SSOGumGroupsClient) Component.getInstance("ssoGroupClient");
        return new SSOGumGroupClientCache(groupService);
    }
}
