package net.ihe.gazelle.ssov7.gum.client.interlay.client.utils;

import net.ihe.gazelle.ssov7.gum.client.application.service.SortOrder;

public class UserSearchParams {

    private String search;
    private String firstName;
    private String lastName;
    private String email;
    private String organizationId;
    private String group;
    private Boolean activated;
    Boolean delegated;
    private String externalId;
    private String idpId;
    private Integer offset;
    private Integer limit;
    String sortBy;
    SortOrder sortOrder;

    public UserSearchParams() {
        //Constructor used for chaining setters.
    }

    public String getSearch() {
        return search;
    }

    public UserSearchParams setSearch(String search) {
        this.search = search;
        return this;
    }

    public String getFirstName() {
        return firstName;
    }

    public UserSearchParams setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public String getLastName() {
        return lastName;
    }

    public UserSearchParams setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public UserSearchParams setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getOrganizationId() {
        return organizationId;
    }

    public UserSearchParams setOrganizationId(String organizationId) {
        this.organizationId = organizationId;
        return this;
    }

    public String getGroup() {
        return group;
    }

    public UserSearchParams setGroup(String group) {
        this.group = group;
        return this;
    }

    public Boolean getActivated() {
        return activated;
    }

    public UserSearchParams setActivated(Boolean activated) {
        this.activated = activated;
        return this;
    }

    public Boolean getDelegated() {
        return delegated;
    }

    public UserSearchParams setDelegated(Boolean delegated) {
        this.delegated = delegated;
        return this;
    }

    public String getExternalId() {
        return externalId;
    }

    public UserSearchParams setExternalId(String externalId) {
        this.externalId = externalId;
        return this;
    }

    public String getIdpId() {
        return idpId;
    }

    public UserSearchParams setIdpId(String idpId) {
        this.idpId = idpId;
        return this;
    }

    public Integer getOffset() {
        return offset;
    }

    public UserSearchParams setOffset(Integer offset) {
        this.offset = offset;
        return this;
    }

    public Integer getLimit() {
        return limit;
    }

    public UserSearchParams setLimit(Integer limit) {
        this.limit = limit;
        return this;
    }

    public String getSortBy() {
        return sortBy;
    }

    public UserSearchParams setSortBy(String sortBy) {
        this.sortBy = sortBy;
        return this;
    }

    public SortOrder getSortOrder() {
        return sortOrder;
    }

    public UserSearchParams setSortOrder(SortOrder sortOrder) {
        this.sortOrder = sortOrder;
        return this;
    }
}
