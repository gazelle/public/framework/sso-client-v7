package net.ihe.gazelle.ssov7.gum.client.interlay.client.utils;

import org.apache.http.client.HttpRequestRetryHandler;
import org.apache.http.protocol.HttpContext;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;

public class SsoServerErrorRetryHandler implements HttpRequestRetryHandler {
    private static final Logger LOG = LoggerFactory.getLogger(SsoServerErrorRetryHandler.class);

    private final int maxRetries;

    public SsoServerErrorRetryHandler(int maxRetries) {
        this.maxRetries = maxRetries;
    }

    @Override
    public boolean retryRequest(IOException exception, int executionCount, HttpContext context) {
        LOG.error("Retrying to connect to the server");
        return executionCount<=maxRetries;
    }
}
