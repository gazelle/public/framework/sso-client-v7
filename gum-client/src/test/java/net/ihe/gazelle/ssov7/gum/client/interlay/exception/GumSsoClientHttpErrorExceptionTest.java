package net.ihe.gazelle.ssov7.gum.client.interlay.exception;

import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNull;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 13/09/2023
 */
public class GumSsoClientHttpErrorExceptionTest extends GumSsoClientHttpErrorException {

    private static final String message = "Test Exception";
    private static final long serialVersionUID = -1487301872849655011L;
    private final Throwable throwable = new Throwable(message);
    
    @Test
    public void testGumSsoClientHttpErrorExceptionWithMessage() {
        GumSsoClientHttpErrorException exception = new GumSsoClientHttpErrorException(message);
        assertEquals(message, exception.getMessage());
    }

    @Test
    public void testGumSsoClientHttpErrorExceptionWithThrowable() {
        GumSsoClientHttpErrorException exception = new GumSsoClientHttpErrorException(throwable);
        assertEquals(exception.getCause(), throwable);
    }

    @Test
    public void testGumSsoClientHttpErrorExceptionSuper() {
        GumSsoClientHttpErrorException exception = new GumSsoClientHttpErrorException();
        assertNull(exception.getMessage());
    }

    @Test
    public void testGumSsoClientHttpErrorExceptionWithMessageAndThrowable() {
        GumSsoClientHttpErrorException exception = new GumSsoClientHttpErrorException(message, throwable);
        assertEquals(exception.getCause(), throwable);
        assertEquals(message, exception.getMessage());
    }

}