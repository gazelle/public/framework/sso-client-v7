package net.ihe.gazelle.ssov7.gum.client.application;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class UserPreferenceTest {

    @Test
    public void getterSetterTest() {

        String userId = "userId";
        String tableLabel = "tableLabel";
        boolean notifiedByEmail = false;
        String profileThumbnailUri = "profileThumbnailUri";
        String profilePictureUri = "profilePictureUri";
        List<String> list = Arrays.asList("en", "fr");

        UserPreference userPreference = getUserPreference(userId, tableLabel, notifiedByEmail, profileThumbnailUri, profilePictureUri, list);

        assertEquals(userId, userPreference.getUserId());
        assertEquals(tableLabel, userPreference.getTableLabel());
        assertEquals(notifiedByEmail, userPreference.isNotifiedByEmail());
        assertEquals(profileThumbnailUri, userPreference.getProfileThumbnailUri());
        assertEquals(profilePictureUri, userPreference.getProfilePictureUri());
        assertEquals(list, userPreference.getLanguagesSpoken());
    }

    @Test
    public void copyConstructorTest() {

        UserPreference userPreference = getUserPreference("userId", "roundTable", true,
                "http://my-uri.fake/picture?format=thumbnail",
                "http://my-uri.fake/picture?format=normal",
                Arrays.asList("en", "fr", "it"));

        UserPreference copy = new UserPreference(userPreference);

        assertEquals(userPreference, copy);
    }

    @Test
    public void equalsAndHashcodeTest() {
        EqualsVerifier.forClass(UserPreference.class)
                .usingGetClass().suppress(Warning.NONFINAL_FIELDS).verify();
    }

    private UserPreference getUserPreference(String userId, String tableLabel, boolean notifiedByEmail, String profileThumbnailUri, String profilePictureUri, List<String> list) {
        UserPreference userPreference = new UserPreference();
        userPreference.setUserId(userId);
        userPreference.setTableLabel(tableLabel);
        userPreference.setNotifiedByEmail(notifiedByEmail);
        userPreference.setProfileThumbnailUri(profileThumbnailUri);
        userPreference.setProfilePictureUri(profilePictureUri);
        userPreference.setLanguagesSpoken(list);
        return userPreference;
    }


}