package net.ihe.gazelle.ssov7.gum.client.interlay.client;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.application.service.SortOrder;
import net.ihe.gazelle.ssov7.gum.client.application.service.UserService;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.ssov7.gum.client.interlay.exception.GumSsoClientHttpErrorException;
import net.ihe.gazelle.ssov7.gum.client.utils.FakeJwtGenerator;
import net.ihe.gazelle.ssov7.m2m.client.interlay.AccessTokenRequestInterceptor;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;
import org.junit.runner.RunWith;
import org.powermock.api.mockito.PowerMockito;
import org.powermock.core.classloader.annotations.PowerMockIgnore;
import org.powermock.core.classloader.annotations.PrepareForTest;
import org.powermock.modules.junit4.PowerMockRunner;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.NoSuchElementException;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static org.junit.Assert.*;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 01/09/2023
 */

@RunWith(PowerMockRunner.class)
//Ignore needed for WireMockRule to work
@PowerMockIgnore({"org.xml.*", "javax.net.ssl.*", "javax.xml.*", "java.xml.*", "javax.security.*", "com.github.tomakehurst.*"})
@PrepareForTest({M2MKeycloakConfigurationServiceImpl.class, AccessTokenRequestInterceptor.class})
public class SSOGumUserClientTest {

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().dynamicPort());
    private UserService userService;
    private HttpClient client;
    private String jsonUsers;
    private String jsonUser;
    private String jsonUsersPage1;
    private String jsonUsersPage2;
    private String jsonUsersPage3;
    private String count;

    private String readTestResourceFile(String file) {
        String resourceStringPath = this.getClass().getResource(file).getPath();
        try {
            return new String(Files.readAllBytes(Paths.get(resourceStringPath)), StandardCharsets.UTF_8);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Before
    public void setUp() throws Exception {
        String baseUrl = wireMockRule.baseUrl();
        environmentVariables.set("GUM_REST_API_URL", baseUrl + "/gum");

        M2MKeycloakConfigurationServiceImpl m2MConfigurationService = PowerMockito.mock(M2MKeycloakConfigurationServiceImpl.class);
        //Mocking all new instances of KeycloakConfigurationServiceImpl
        PowerMockito.whenNew(M2MKeycloakConfigurationServiceImpl.class)
                .withNoArguments()
                .thenReturn(m2MConfigurationService);
        when(m2MConfigurationService.getM2MClientId()).thenReturn("m2M-test-78459s");
        when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn(baseUrl);
        environmentVariables.set("GZL_M2M_CLIENT_SECRET", "secret");
        client = mock(HttpClient.class);
        String token = new FakeJwtGenerator().generateFakeJwt(System.currentTimeMillis() / 1000);
        wireMockRule.stubFor(post("/realms/gazelle/protocol/openid-connect/token")
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + token + "\"}")
                ));

        jsonUser = readTestResourceFile("/getOneUser.json");
        jsonUsers = readTestResourceFile("/getUsers.json");
        count = readTestResourceFile("/count.json");
        jsonUsersPage1 = readTestResourceFile("/getUsersPage1.json");
        jsonUsersPage2 = readTestResourceFile("/getUsersPage2.json");
        jsonUsersPage3 = readTestResourceFile("/getUsersPage3.json");

        userService = new SSOGumUserClient();
    }

    @Test
    public void testFilterUsersWithoutParameter() {
        stubFor(get("/gum/rest/users").willReturn(aResponse().withBody(jsonUsers)));
        UserSearchParams userSearchParams = new UserSearchParams();
        assertNotNull(userService.searchAndFilter(userSearchParams));
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/users")));
        assertEquals(3, userService.searchAndFilter(userSearchParams).getUsers().size());
    }

    @Test
    public void testFilterUsersWithOneParam() {
        stubFor(get("/gum/rest/users?firstName=user1").willReturn(aResponse().withBody(jsonUsers)));
        UserSearchParams userSearchParams = new UserSearchParams().setFirstName("user1");
        assertNotNull(userService.searchAndFilter(userSearchParams));
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/users?firstName=user1")));
    }

    @Test
    public void testFilterUsersWithTwoParams() {
        stubFor(get("/gum/rest/users?firstName=user1&organizationId=Institution1").willReturn(aResponse().withBody(jsonUsers)));
        UserSearchParams userSearchParams = new UserSearchParams().setFirstName("user1").setOrganizationId("Institution1");
        assertNotNull(userService.searchAndFilter(userSearchParams));
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/users?firstName=user1&organizationId=Institution1")));
    }

    @Test
    public void testFilterUsersWithThreeParams() {
        stubFor(get("/gum/rest/users?lastName=user1&activated=true&offset=3").willReturn(aResponse().withBody(jsonUsers)));
        UserSearchParams userSearchParams = new UserSearchParams().setLastName("user1").setActivated(true).setOffset(3);
        assertNotNull(userService.searchAndFilter(userSearchParams));
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/users?lastName=user1&activated=true&offset=3")));
    }

    @Test
    public void testFilterUsersWithAllParams() {
        stubFor(get("/gum/rest/users?firstName=firstname&lastName=lastname&organizationId=Institution1&group=gazelle_admin&activated=true&delegated=false&externalId=555&idpId=58&offset=3&limit=3&sortBy=lastName&sortOrder=ASC").willReturn(aResponse().withBody(jsonUsers)));
        UserSearchParams userSearchParams = new UserSearchParams().setFirstName("firstname")
                .setLastName("lastname")
                .setOrganizationId("Institution1")
                .setGroup("gazelle_admin")
                .setActivated(true)
                .setDelegated(false)
                .setExternalId("555").setIdpId("58")
                .setOffset(3).setLimit(3).setSortBy("lastName").setSortOrder(SortOrder.ASC);
        assertNotNull(userService.searchAndFilter(userSearchParams));
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/users?firstName=firstname&lastName=lastname&organizationId=Institution1&group=gazelle_admin&activated=true&delegated=false&externalId=555&idpId=58&offset=3&limit=3&sortBy=lastName&sortOrder=ASC")));
    }

    @Test
    public void testFilterUsersThrowsGumSsoUnknownErrorException() {
        stubFor(get("/gum/rest/users").willReturn(null));
        try {
            UserSearchParams userSearchParams = new UserSearchParams();
            userService.searchAndFilter(userSearchParams);
        } catch (GumSsoClientHttpErrorException e) {
            assertEquals("searchAndFilter failed", e.getMessage());
        }
    }

    @Test
    public void testFilterUsersThrowsGumSsoClientHttpErrorException() {
        HttpGet get = new HttpGet("http://localhost:8080/gum/rest/users");
        stubFor(get("/gum/rest/users").willReturn(null));
        try {
            when(client.execute(get)).thenThrow(new GumSsoClientHttpErrorException("searchAndFilter failed"));
            UserSearchParams userSearchParams = new UserSearchParams();
            userService.searchAndFilter(userSearchParams);
        } catch (GumSsoClientHttpErrorException e) {
            assertEquals("searchAndFilter failed", e.getMessage());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    @Test
    public void testFilterUsersThrowsBadRequestException() {
        stubFor(get("/gum/rest/users").willReturn(badRequest()));
        try {
            UserSearchParams userSearchParams = new UserSearchParams();
            userService.searchAndFilter(userSearchParams);
        } catch (GumSsoClientHttpErrorException e) {
            assertEquals("400 Bad Request", e.getMessage());
        }
    }

    @Test
    public void testGetValueCount() {
        String organizationId = "IHE";
        String propertyName = "organizationId";
        stubFor(get("/gum/rest/users/" + propertyName + "/count?organizationId=" + organizationId).willReturn(aResponse().withBody(count)));
        assertNotNull(userService.getValueCount(propertyName, null, organizationId, null, null));
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/users/organizationId/count?organizationId=IHE")));
    }

    @Test
    public void testGetValueCountThrowsGumSsoClientHttpErrorException() {
        String organizationId = "IHE";
        String propertyName = "organizationId";
        HttpGet get = new HttpGet("http://localhost:8080/gum/rest/users");
        stubFor(get("/gum/rest/users/" + propertyName + "/count?organizationId=" + organizationId).willReturn(aResponse().withBody(count)));
        try {
            when(client.execute(get)).thenThrow(new GumSsoClientHttpErrorException("getValueCount failed"));
            userService.getValueCount(propertyName, null, organizationId, null, null);
        } catch (GumSsoClientHttpErrorException | IOException e) {
            assertEquals("getValueCount failed", e.getMessage());
        }
    }

    @Test
    public void testGetUserById() {
        stubFor(get(urlMatching("/gum/rest/users/user2")).willReturn(aResponse().withBody(jsonUser)));
        assertNotNull(userService.getUserById("user2"));
        verify(1, getRequestedFor(urlEqualTo("/gum/rest/users/user2")));
        assertEquals("user2@gazelle.com", userService.getUserById("user2").getEmail());
    }

    @Test
    public void testGetUserByIdThrowsBadRequestException() {
        stubFor(get("/gum/rest/users/user2").willReturn(badRequest()));
        try {
            userService.getUserById("user2");
        } catch (GumSsoClientHttpErrorException e) {
            assertEquals("400 Bad Request", e.getMessage());
        }
    }

    @Test
    public void testGetUserByIdThrowsClientHttpErrorException() {
        HttpGet get = new HttpGet("http://localhost:9090/gum/rest/users/users2");
        stubFor(get("/gum/rest/users/user2"));
        try {
            when(client.execute(get)).thenThrow(new GumSsoClientHttpErrorException("Can't create client"));
            userService.getUserById("user2");
        } catch (GumSsoClientHttpErrorException | IOException e) {
            assertEquals("getUserById failed with userId user2", e.getMessage());
        }
    }

    @Test
    public void testGetUserByIdThrowsNotFoundException() {
        stubFor(get("/gum/rest/users/user9").willReturn(status(404).withBody(jsonUser)));
        try {
            userService.getUserById("user9");
        } catch (NoSuchElementException e) {
            assertEquals("404 Not Found", e.getMessage());
        }
    }

    @Test
    public void testGetUserByIdThrowsUnknownErrorException() {
        stubFor(get("/gum/rest/users/user2").willReturn(null));
        try {
            userService.getUserById("user2");
        } catch (GumSsoClientHttpErrorException e) {
            assertEquals("getUserById failed with userId user2", e.getMessage());
        }
    }

    @Test
    public void testGetUserByIdThrowsNullRequestException() {
        stubFor(get("/gum/rest/users/user2"));
        try {
            userService.getUserById(null);
        } catch (IllegalArgumentException e) {
            assertTrue(e.getMessage().contains("userId is null"));
        }
    }

    @Test
    public void testFilterNoLimit() {
        stubFor(get("/gum/rest/users?activated=true&offset=0&limit=3").willReturn(aResponse().withBody(jsonUsersPage1)));
        stubFor(get("/gum/rest/users?activated=true&offset=3&limit=3").willReturn(aResponse().withBody(jsonUsersPage2)));
        stubFor(get("/gum/rest/users?activated=true&offset=6&limit=3").willReturn(aResponse().withBody(jsonUsersPage3)));
        userService = new SSOGumUserClient(3);

        UserSearchParams userSearchParams = new UserSearchParams().setActivated(true);
        List<User> users = userService.searchNoLimit(userSearchParams);

        assertEquals(7, users.size());
    }

    @Test
    public void getUserDisplayNameWithoutException() {
        String notFoundUser = "notFoundUser";
        stubFor(get(urlMatching("/gum/rest/users/" + notFoundUser)).
                willReturn(notFound()));

        assertEquals("Unknown user", userService.getUserDisplayNameWithoutException(notFoundUser));
    }

    @Test
    public void getUserDisplayNameWithoutExceptionIdNullOrEmpty() {
        assertEquals("Unknown user", userService.getUserDisplayNameWithoutException(null));
        assertEquals("Unknown user", userService.getUserDisplayNameWithoutException(""));
    }
}