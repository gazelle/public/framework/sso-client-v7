package net.ihe.gazelle.ssov7.gum.client.interlay.cache;

import net.ihe.gazelle.ssov7.gum.client.application.User;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.SSOGumUserClient;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.utils.UserSearchParams;
import net.ihe.gazelle.ssov7.gum.client.interlay.model.entity.UserSearchResult;
import org.junit.Before;
import org.junit.Test;

import java.sql.Timestamp;
import java.util.*;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 12/10/2023
 */
public class SSOGumUserClientCacheTest {

    private SSOGumUserClientCache cache;
    private SSOGumUserClient userClient;
    private UserSearchResult userSearchResult;
    private User user;

    @Before
    public void init() {
        userClient = mock(SSOGumUserClient.class);
        cache = new SSOGumUserClientCache(userClient);
        Set<String> groups = new HashSet<>(Arrays.asList("vendor", "vendor_admin"));
        user = new User("user", "user fn", "user ln", "user@lost.com", groups, "groupId", true, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        User user2 = new User("user2", "user2 fn", "user2 ln", "user2@lost.com", groups, "groupId", true, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        User user3 = new User("user3", "user3 fn", "user3 ln", "user3@lost.com", groups, "groupId", true, null, null, new Timestamp(1601906470000L), 0, new Timestamp(1601906470000L));
        List<User> users = new ArrayList<>();
        users.addAll(Arrays.asList(user, user2, user3));
        userSearchResult = new UserSearchResult(users, null, null, null);
    }

    @Test
    public void testGetUserById() {
        when(userClient.getUserById("user1")).thenReturn(this.user);
        User cachedUser = cache.getUserById("user1");
        assertNotNull(cachedUser);
        assertEquals("user fn", cachedUser.getFirstName());
        User newUser = cache.getUserById("user1");
        assertEquals("user fn", newUser.getFirstName());
    }


    @Test
    public void testSearchAndFilterUsers() {
        UserSearchParams userSearchParams = new UserSearchParams();
        when(userClient.searchAndFilter(userSearchParams)).thenReturn(userSearchResult);
        List<User> userList = cache.searchAndFilter(userSearchParams).getUsers();
        assertNotNull(userList);
        assertEquals(userSearchResult.getUsers().size(), userList.size());
        List<User> newUserList = cache.searchAndFilter(userSearchParams).getUsers();
        assertNotNull(newUserList);
        assertEquals(userSearchResult.getUsers().size(), newUserList.size());
    }

    @Test
    public void getUserDisplayNameWithoutException() {
        assertEquals("Unknown user", cache.getUserDisplayNameWithoutException("notFoundUser"));
    }

    @Test
    public void getUserDisplayNameWithoutExceptionIdNullOrEmpty() {
        assertEquals("Unknown user", cache.getUserDisplayNameWithoutException(null));
        assertEquals("Unknown user", cache.getUserDisplayNameWithoutException(""));
    }

}