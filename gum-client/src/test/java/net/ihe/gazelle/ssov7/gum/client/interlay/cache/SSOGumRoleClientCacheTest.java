package net.ihe.gazelle.ssov7.gum.client.interlay.cache;

import net.ihe.gazelle.ssov7.gum.client.application.Group;
import net.ihe.gazelle.ssov7.gum.client.interlay.client.SSOGumGroupsClient;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

/**
 * @author Claude LUSSEAU
 * @company KEREVAL
 * @project sso-client-v7
 * @date 12/10/2023
 */
public class SSOGumRoleClientCacheTest {

    private SSOGumGroupClientCache cache;
    private SSOGumGroupsClient roleClient;
    private final List<Group> globalGroupList = new ArrayList<>();

    @Before
    public void init() {
        roleClient = mock(SSOGumGroupsClient.class);
        cache = new SSOGumGroupClientCache(roleClient);
        globalGroupList.addAll(Arrays.asList(new Group("role", "gazelle_admin"),
                new Group("role", "monitor"),
                new Group("role", "Project manager"),
                new Group("org", "organization"),
                new Group("org-adm", "organization"),
                new Group("role","test_designer"),
                new Group("role","Role to allow the user to modify the content of a system or add or delete a system")));
    }

    @Test
    public void getGroupsTest() {
        when(roleClient.getGroups()).thenReturn(this.globalGroupList);
        List<Group> groupList = cache.getGroups();
        assertNotNull(groupList);
        assertEquals(this.globalGroupList.size(), groupList.size());
        List<Group> newRoleList = cache.getGroups();
        assertNotNull(newRoleList);
        assertEquals(this.globalGroupList.size(), newRoleList.size());
    }
}