# Gum Client

This library is used by Gazelle web applications to manage users via REST access
points exposed by Gazelle User Management (GUM).

<!-- TOC -->
* [Gum Client](#gum-client)
  * [Dependency](#dependency)
  * [Environment Variables](#environment-variables)
  * [Gum Client usage](#gum-client-usage)
    * [Call interface](#call-interface)
    * [Cache management](#cache-management)
    * [Interface methods](#interface-methods)
<!-- TOC -->

## Dependency

To use the gum client, add as dependency in your ejb:

 ```xml
<dependency>
    <groupId>net.ihe.gazelle</groupId>
    <artifactId>gum-client</artifactId>
    <version>${sso.client.v7.version}</version>
</dependency>
 ```

## Environment Variables

| Name                              | Required | Description                                          | Default                         | Value Example                       |
|-----------------------------------|----------|------------------------------------------------------|---------------------------------|-------------------------------------|
| GUM_REST_API_URL                  | TRUE     | Gum service end point                                | none                            | https://my.domain.com/gum           |
| SSO_CLIENT_RETRY_INTERVAL         | FALSE    | Retry interval between request when server error     | 30 sec                          | 40                                  |
| GZL_USER_CACHE_EXPIRATION_MINUTES | FALSE    | Cache expiration                                     | 15 min                          | 20                                  |
| GUM_REGISTRATION_URL              | TRUE     | Url of the Gazelle User Management registration page | "${GUM_FRONT_URL}/registration" | "https://my-gum-front/registration" |
| GUM_ACCOUNT_URL                   | TRUE     | Url of the Gazelle User Management account page      | "${GUM_FRONT_URL}/account"      | "https://my-gum-front/account"      |
| GUM_USERS_URL                     | TRUE     | Url of the Gazelle User Management users page        | "${GUM_FRONT_URL}/users"        | "https://my-gum-front/users"        |

## Application preferences

| Name          | Description | Value Example                |
|---------------|-------------|------------------------------|
| gum_front_url | URL         | http://localhost:8001/gum-ui |


## Application preferences

| Name          | Description | Value Example                |
|---------------|-------------|------------------------------|
| gum_front_url | URL         | http://localhost:8001/gum-ui |


## Gum Client usage

### Call interface

You can inject the gum client interface using org.jboss.seam.annotations:

```java
@In(value = "gumUserService")
private transient UserService userService;
```

Or using org.jboss.seam:

```java
UserService service = (UserService) Component.getInstance("gumUserService");
```

### Cache management

By default, when the UserService interface is called, the UserServiceFactory class is called via the "gumUserService"
annotation.
The latter automatically applies cache management.

```java
@Factory(value = "gumUserService", autoCreate = true, scope = ScopeType.APPLICATION)
public static UserService getUserService() {
    if (userServiceCache == null) {
        userServiceCache = new SSOGumUserClientCache(new SSOGumUserClient());
    }
    return userServiceCache;
}
```

### Interface methods

The UserService interface offers several methods, as follows:

- #### *editUser:*

This method calls the GUM endpoint to edit a user with PATCH request as follows:

```http request
/gum/rest/users/{userId}
```

with body like:

```json
{
  "firstName": "John",
  "lastName": "Doe",
  "organizationId": "IHE"
}
```

- #### *searchAndFilter:*

This method is used for filters with GET request.
Search for users according to criteria.  
This method return the search result that contains the selected offset, the limit, the total count of matches and the
List of
all users corresponding to the defined filter parameters for the selected page (offset and limit).

- #### *searchNoLimit*:

Same as [searchAndFilter](#searchandfilter), but without pagination and return a
list of all users corresponding to the defined filter parameters.
> :warning: **Warning:**  
> This can take a lot of time and resources if filtering criteria are not restrictive enough.

- #### *getValueCount:*

This method is used for getting distinct possibles values of a property with GET request and
a String-Long map containing as key the existing distincts values for the given property and as value the
count of all filtered users having this value.

- #### *getUserById:*

This method calls the GUM endpoint to find user with GET request
and return the user found with the given id returned of User.

```http request
/gum/rest/users/{userId}
```

- #### *deactivate:*

This method calls the GUM endpoint to deactivate user with POST request
and with activationResource object parameters of the user to be edited.

```http request
/gum/rest/users/deactivate
```

- #### *activateUser (with activationResource):*

This method calls the GUM endpoint to activate user with POST request
and with activationResource of the user to be activated.

- #### *activateUser (with activationCode):*

This method calls the GUM endpoint to activate user with POST request
and with activationCode of the user to be activated.

- #### *removeUserCache:*

This method call the implementation who remove the user cache according to the user id.

- #### *getUserDisplayNameWithoutException:*

Return the display name of the user or the user id if the display name is not found.
