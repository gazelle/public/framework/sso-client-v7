# Cas client UI v7

<!-- TOC -->
* [Cas client UI v7](#cas-client-ui-v7)
  * [Single SSO server usage](#single-sso-server-usage)
<!-- TOC -->

This module contains JSF component used to add login button and dropdown menu.

>:warning: **Warning:**
> 
> For CAS authentication to work, this module must be paired with the cas-client-v7 module.
> 
> [See here](../cas-client-v7/README.md) for more information.

## Single SSO server usage

1. To use the provided login menu/buttons in JSF, add as dependency in your war
    
   ```xml
    <dependency>
         <groupId>net.ihe.gazelle</groupId>
         <artifactId>cas-client-ui-v7</artifactId>
         <version>${sso.client.v7.version}</version>
         <type>war</type>
     </dependency>
    ```
   
2. Update your pages.xml like this:

    ```xml
    <page view-id="/login/logout.xhtml" action="#{identity.ssoLogout}">
        <navigation>
            <rule if-outcome="casLogout">
                <redirect view-id="/cas/logout.xhtml"/>
            </rule>
            <rule if-outcome="basicLogout">
                <redirect view-id="/home.xhtml"/>
            </rule>
            <rule if-outcome="noLogout">
                <redirect view-id="/home.xhtml"/>
            </rule>
        </navigation>
    </page>
    <page view-id="/login/ipLogin.xhtml" action="#{identity.login()}">
        <navigation>
            <rule>
                <redirect view-id="/home.xhtml"/>
            </rule>
        </navigation>
    </page>
    <page view-id="/userPhoto.xhtml" action="#{gumUserProfilePictureBean.getUserProfilePicture()}"/>

    ```

3. Create default login/logout buttons in navbar like this:

    ```xml
    <ui:include src="/layout/_login_menu.xhtml"/>
    ```
   Or, if you want to customize the login menu content:

    ```xml
    <ui:decorate template="/layout/_login_menu.xhtml">
       <ui:define name="loggedInPrefix">
           <!-- Prefix added before user's names in the login button -->
           <s:span id="userPicMin" title="#{identity.username}">
               <h:graphicImage class="gzl-menu-image img-rounded"
                               value="#{gumUserProfilePictureBean.getUserProfileThumbnailUri(identity.username)}"/>
           </s:span>
       </ui:define>
       <ui:define name="loggedInMenuExtension">
           <!-- added menu entries on top of logout entry -->
           <li>
               <h:outputLink
                       value="/#{applicationPreferenceManager.getApplicationUrlBaseName()}/users/user/userPreferences.seam">
                   <h:outputText value="#{messages['net.ihe.gazelle.tm.Preferences']}"/>
               </h:outputLink>
           </li>
           <li class="divider"/>
       </ui:define>
    </ui:decorate>
    ```