package net.ihe.gazelle.cas.client.authentication;

import org.jasig.cas.client.authentication.AttributePrincipal;
import org.jasig.cas.client.util.AbstractCasFilter;
import org.jasig.cas.client.validation.Assertion;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Timestamp;
import java.util.Collections;
import java.util.Map;

public class SessionExpirationFilter implements Filter {

    @Override
    public void init(FilterConfig filterConfig) {
        // Nothing to do here
    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        if (servletRequest instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
            HttpSession session = httpServletRequest.getSession(false);

            // Retrieve user ticket attributes
            Map<String, Object> attributes = getTicketAttributesFromSession(session);
            if (!attributes.isEmpty()) {
                Object ticketSessionMaxDurationSecondsObject = attributes.get("ticket_session_max_duration_seconds");
                Object authenticationTimestampObject = attributes.get("authentication_timestamp");
                if (ticketSessionMaxDurationSecondsObject != null) {
                    //Updating the current session inactive interval with value from CAS ticket
                    int ticketSessionMaxDurationSeconds = Integer.parseInt((String) ticketSessionMaxDurationSecondsObject);
                    session.setMaxInactiveInterval(ticketSessionMaxDurationSeconds);

                    // If attributes no found in ticket, do nothing
                    if (authenticationTimestampObject != null) {
                        // Parse attribute
                        Timestamp authenticationTimestamp = Timestamp.valueOf((String) authenticationTimestampObject);

                        // Check if max duration is reached
                        if (sessionShouldBeInvalidated(authenticationTimestamp, ticketSessionMaxDurationSeconds)) {
                            invalidateSession(httpServletRequest, session);
                        }
                    }
                }
            }
        }
        filterChain.doFilter(servletRequest, servletResponse);
    }

    @Override
    public void destroy() {
        // Nothing to do here
    }

    private Map<String, Object> getTicketAttributesFromSession(HttpSession session) {
        Assertion assertion = (Assertion) session.getAttribute(AbstractCasFilter.CONST_CAS_ASSERTION);
        // Check if user is authenticated
        if (assertion != null && assertion.isValid()) {
            AttributePrincipal attributePrincipal = assertion.getPrincipal();
            if (attributePrincipal != null) {
                return attributePrincipal.getAttributes();
            }
        }
        return Collections.emptyMap();
    }

    private boolean sessionShouldBeInvalidated(Timestamp authenticationTimestamp, int ticketSessionMaxDurationSeconds) {
        Timestamp timestamp = new Timestamp(System.currentTimeMillis());
        // Check if max duration is reach
        return authenticationTimestamp.getTime() + (ticketSessionMaxDurationSeconds * 1000L) < timestamp.getTime();
    }

    private void invalidateSession(HttpServletRequest httpServletRequest, HttpSession session) {
        // Invalidate current session
        httpServletRequest.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
        session.setAttribute(AbstractCasFilter.CONST_CAS_ASSERTION, null);
        session.invalidate();
    }
}
