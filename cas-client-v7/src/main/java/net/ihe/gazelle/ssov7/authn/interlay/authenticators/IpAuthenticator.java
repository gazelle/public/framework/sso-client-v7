package net.ihe.gazelle.ssov7.authn.interlay.authenticators;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorResource;
import net.ihe.gazelle.ssov7.authn.interlay.adapter.IpPrincipal;
import net.ihe.gazelle.ssov7.authn.interlay.filter.IpAuthenticationFilter;
import net.ihe.gazelle.ssov7.authn.interlay.helper.AuthenticatorHelper;
import org.jboss.seam.web.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

public class IpAuthenticator implements Authenticator {
    private static final Logger LOG = LoggerFactory.getLogger(IpAuthenticator.class);
    private static final String DEFAULT_ADMIN_USERNAME = "admin";
    private static final String DEFAULT_ADMIN_FIRSTNAME = "Admin";
    private static final String DEFAULT_ADMIN_LASTNAME = "Admin";
    private static final String DEFAULT_ADMIN_EMAIL = "admin@gazelle.com";
    private static final String DEFAULT_ADMIN_INSTITUTION = "ADMIN_ORG";

    private static final String DEFAULT_ADMIN_ROLE = "admin_role";
    private final PreferenceProvider preferenceProvider;
    private final AuthenticatorHelper authenticatorHelper;
    public IpAuthenticator(PreferenceProvider preferenceProvider) {
        this.preferenceProvider = preferenceProvider;
         authenticatorHelper = new AuthenticatorHelper();
    }

    @Override
    public AuthenticatorResource authenticate() {
        LOG.warn("Login via IP address is enabled." +
                " This is a security risk and must not be used on public or un-secured networks.");
        AuthenticatorResource authenticatorResource = new AuthenticatorResource();
        final HttpServletRequest request = authenticatorHelper.getHttpServletRequestFromFaceContext();
        if (isReadyToLogin() && request != null) {
            final String remoteAddr = request.getRemoteAddr();
            authenticatorResource.setPrincipal(new IpPrincipal(DEFAULT_ADMIN_USERNAME, remoteAddr));

            authenticatorResource.setUsername(DEFAULT_ADMIN_USERNAME);
            authenticatorResource.setOrganizationKeyword(DEFAULT_ADMIN_INSTITUTION);
            authenticatorResource.setRoles(DEFAULT_ADMIN_ROLE);
            authenticatorResource.setFirstName(DEFAULT_ADMIN_FIRSTNAME);
            authenticatorResource.setLastName(DEFAULT_ADMIN_LASTNAME);
            authenticatorResource.setEmail(DEFAULT_ADMIN_EMAIL);

            LOG.info("{} has logged in from {}", authenticatorResource.getUsername(), remoteAddr);
            authenticatorResource.setLoggedIn(true);
            return authenticatorResource;
        }
        authenticatorResource.setLoggedIn(false);
        return authenticatorResource;
    }


    @Override
    public boolean isReadyToLogin() {

        HttpServletRequest httpServletRequest = authenticatorHelper.getHttpServletRequestFromFaceContext();
        if (httpServletRequest != null) {
            final String remoteAddr = httpServletRequest.getRemoteAddr();
            String ipRegexp = preferenceProvider.getString("ip_login_admin");
            if (ipRegexp == null)
                ipRegexp = ".*";
            Boolean ipLogged = (Boolean) httpServletRequest.getAttribute(IpAuthenticationFilter.CONST_IP_LOGIN);
            if (ipLogged==null){
                HttpSession session = httpServletRequest.getSession();
                if (session != null) {
                    ipLogged= (Boolean) session.getAttribute(IpAuthenticationFilter.CONST_IP_LOGIN);
                }
            }
            return remoteAddr.matches(ipRegexp) && Boolean.TRUE.equals(ipLogged);
        }
        return false;
    }

    @Override
    public String getLogoutType() {
        return "basicLogout";
    }

    @Override
    public void logout() {
        final HttpServletRequest request = authenticatorHelper.getHttpServletRequestFromFaceContext();
        if (request != null) {
            request.setAttribute(IpAuthenticationFilter.CONST_IP_LOGIN, false);
            final HttpSession session = request.getSession(false);
            session.setAttribute(IpAuthenticationFilter.CONST_IP_LOGIN, false);
        }
        Session.instance().invalidate();
    }


}
