package net.ihe.gazelle.ssov7.authn.interlay.filter;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import org.jasig.cas.client.authentication.AuthenticationRedirectStrategy;
import org.jasig.cas.client.authentication.DefaultAuthenticationRedirectStrategy;
import org.jboss.seam.contexts.Lifecycle;
import org.jboss.seam.web.AbstractFilter;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class IpAuthenticationFilter extends AbstractFilter {

    private PreferenceProvider preferenceProvider;
    public static final String CONST_IP_LOGIN = "_const_ip_login_";

    @Override
    public void init(FilterConfig filterConfig) {
        preferenceProvider = GenericServiceLoader.getService(PreferenceProvider.class);
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
        if (request instanceof HttpServletRequest) {
            HttpServletRequest httpServletRequest = (HttpServletRequest) request;
            HttpSession session = httpServletRequest.getSession(false);
            // We check that seam context is active because in some application is not the case
            Lifecycle.beginCall();
            if (Boolean.TRUE.equals(preferenceProvider.getBoolean("ip_login"))) {
                request.setAttribute(CONST_IP_LOGIN, true);
                session.setAttribute(CONST_IP_LOGIN, true);
                String redirectUrl = httpServletRequest.getContextPath();
                AuthenticationRedirectStrategy authenticationRedirectStrategy = new DefaultAuthenticationRedirectStrategy();
                authenticationRedirectStrategy.redirect((HttpServletRequest) request, (HttpServletResponse) response, redirectUrl);
            } else {
                chain.doFilter(request, response);
            }
            Lifecycle.endCall();
        } else {
            chain.doFilter(request, response);
        }
    }

    @Override
    public void destroy() {
        // Nothing to do here
    }
}
