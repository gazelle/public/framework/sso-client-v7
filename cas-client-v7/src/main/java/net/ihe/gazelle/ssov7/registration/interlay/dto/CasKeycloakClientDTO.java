package net.ihe.gazelle.ssov7.registration.interlay.dto;


import net.ihe.gazelle.ssov7.authn.interlay.dto.KeycloakClientDTO;

public class CasKeycloakClientDTO extends KeycloakClientDTO {

    public CasKeycloakClientDTO(){
        super("cas");
    }
}
