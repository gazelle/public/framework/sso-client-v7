package net.ihe.gazelle.ssov7.configuration.interlay;

import net.ihe.gazelle.ssov7.configuration.domain.ApplicationConfigurationService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Scope(ScopeType.EVENT)
@AutoCreate
@Name("applicationConfigurationService")
public class ApplicationConfigurationServiceImpl implements ApplicationConfigurationService {
    private static final Logger LOG = LoggerFactory.getLogger(ApplicationConfigurationServiceImpl.class);
    private static final int NEW_LOGIN_MENU_GUM_VERSION = 4;

    private final GumConfigClient gumConfigClient;

    public ApplicationConfigurationServiceImpl() {
        this(new GumConfigClient());
    }

    public ApplicationConfigurationServiceImpl(GumConfigClient gumConfigClient) {
        this.gumConfigClient = gumConfigClient;
    }

    @Override
    public boolean isUserRegistrationEnabled() {
        try {
            return gumConfigClient.getUserRegistrationEnabled();
        } catch (GumConfigClientException e) {
            LOG.error("Could not determine if user registration is enabled. See following error:", e);
            return false;
        }
    }

    @Override
    public String getUserRegistrationUrl() {
        String gumRegistrationUrl = System.getenv("GUM_REGISTRATION_URL");
        if (gumRegistrationUrl == null)
            throw new IllegalStateException("GUM_REGISTRATION_URL not set");
        return gumRegistrationUrl;
    }

    @Override
    public String getUserAccountUrl() {
        String gumAccountUrl = System.getenv("GUM_ACCOUNT_URL");
        if (gumAccountUrl == null)
            throw new IllegalStateException("GUM_ACCOUNT_URL not set");
        return gumAccountUrl;
    }

    @Override
    public String getUsersManagementUrl() {
        String gumUsersUrl = System.getenv("GUM_USERS_URL");
        if (gumUsersUrl == null)
            throw new IllegalStateException("GUM_USERS_URL not set");
        return gumUsersUrl;
    }

    @Override
    public boolean shouldShowNewMenu() {
        Integer gumMajorVersion = gumConfigClient.getGumMajorVersion();
        if (gumMajorVersion == null){
            LOG.error("Could not determine GUM major version, check your GUM configuration");
            return false;
        }
        return gumMajorVersion >= NEW_LOGIN_MENU_GUM_VERSION;
    }
}
