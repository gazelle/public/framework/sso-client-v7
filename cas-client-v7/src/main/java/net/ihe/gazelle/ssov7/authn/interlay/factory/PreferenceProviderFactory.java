package net.ihe.gazelle.ssov7.authn.interlay.factory;

import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;

@Name("preferenceProviderFactory")
@Scope(ScopeType.APPLICATION)
@Install
@AutoCreate
public class PreferenceProviderFactory {

    @Factory(value = "preferenceProvider", scope = ScopeType.EVENT)
    public PreferenceProvider getPreferenceProvider() {
        return GenericServiceLoader.getService(PreferenceProvider.class);
    }
}
