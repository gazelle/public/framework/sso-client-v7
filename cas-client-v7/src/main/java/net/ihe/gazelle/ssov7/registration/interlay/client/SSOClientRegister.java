package net.ihe.gazelle.ssov7.registration.interlay.client;


import net.ihe.gazelle.preferences.PreferenceProvider;
import net.ihe.gazelle.services.GenericServiceLoader;
import net.ihe.gazelle.ssov7.configuration.domain.CasConfigurationService;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@Name("ssoClientRegister")
@AutoCreate
@Scope(ScopeType.APPLICATION)
public class SSOClientRegister {
    private static final Logger LOG = LoggerFactory.getLogger(SSOClientRegister.class);
    private static final String APPLICATION_URL = "application_url";
    private static final String CAS_ENABLED = "cas_enabled";

    @In(value = "keycloakConfigurationService")
    private CasConfigurationService casConfigurationService;

    @Observer("org.jboss.seam.postInitialization")
    public void init() {

        PreferenceProvider preferenceProvider = GenericServiceLoader.getService(PreferenceProvider.class);
        if(Boolean.TRUE.equals(preferenceProvider.getBoolean(CAS_ENABLED))) {
            String clientId = casConfigurationService.getCASClientId();
            LOG.info("Register CAS client {} to SSO server",clientId);

            // Retry interval between request when server error, defaulting with 30 seconds
            String casRegistrationRetryInterval = System.getenv("CAS_REGISTRATION_RETRY_INTERVAL");
            Integer retryInterval = casRegistrationRetryInterval == null ? 30000 : Integer.parseInt(casRegistrationRetryInterval);

            try {
                String keycloakUrl = casConfigurationService.getSSOServerBaseUrl();
                String applicationUrl = preferenceProvider.getString(APPLICATION_URL);

                SSOClientRegistrationService ssoClientRegistration = new KeycloakClientRegistrationService(keycloakUrl, retryInterval);
                ssoClientRegistration.registerClient(clientId,applicationUrl);
            } catch (Exception e) {
                LOG.error("/!\\ Failed to register CAS resources to SSO server. This may leads to further issues.", e);
            }
        }
    }
}
