package net.ihe.gazelle.ssov7.configuration.domain;

public interface ApplicationConfigurationService {

    /**
     * Get if user registration is enabled
     *
     * @return true if user registration is enabled, false otherwise
     */
    boolean isUserRegistrationEnabled();

    /**
     * Gets user registration url.
     *
     * @return the user registration url
     * @throws IllegalStateException if GUM_REGISTRATION_URL environment variable is not set
     */
    String getUserRegistrationUrl();

    /**
     * Gets user account url.
     *
     * @return the user account url
     * @throws IllegalStateException if GUM_ACCOUNT_URL environment variable is not set
     */
    String getUserAccountUrl();

    /**
     * Gets user account url.
     *
     * @return the user account url
     * @throws IllegalStateException if GUM_USERS_URL environment variable is not set
     */
    String getUsersManagementUrl();

    /**
     * Returns true if the deployed Gazelle User Management version is 4 or more, false otherwise.
     * Gazelle User Management step 4 made possible to show an updated menu to all application. This method is only present for
     * retro compatibility purposes.
     *
     * @return true if Gazelle User Management version is 4 or more, false otherwise
     */
    boolean shouldShowNewMenu();
}
