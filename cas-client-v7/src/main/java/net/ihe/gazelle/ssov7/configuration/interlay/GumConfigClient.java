package net.ihe.gazelle.ssov7.configuration.interlay;

import org.apache.http.HttpStatus;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.codehaus.jackson.JsonNode;
import org.codehaus.jackson.map.ObjectMapper;

import java.io.IOException;
import java.io.InputStream;

public class GumConfigClient {

    private String configurationsJson;

    /**
     * Gets user registration enabled.
     *
     * @return true if user registration is enabled, false otherwise
     * @throws GumConfigClientException the value is not accessible
     */
    public boolean getUserRegistrationEnabled() {
        if (configurationsJson == null) {
            try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
                HttpGet getGumConfigurationsRequest = new HttpGet(System.getenv("GUM_REST_API_URL") + "/rest/configurations");
                try (CloseableHttpResponse response = httpClient.execute(getGumConfigurationsRequest)) {
                    assertNoErrorStatus(response);
                    InputStream responseContent = response.getEntity().getContent();
                    setConfigurationJson(responseContent);
                }
            } catch (IOException e) {
                throw new GumConfigClientException("Could not get configurations", e);
            }
        }
        return extractBooleanFieldValueFromJson("userRegistrationEnabled", configurationsJson);
    }

    public Integer getGumMajorVersion() {
        try (CloseableHttpClient httpClient = HttpClients.createDefault()) {
            HttpGet getGumMetadata = new HttpGet(System.getenv("GUM_REST_API_URL") + "/metadata");
            try (CloseableHttpResponse response = httpClient.execute(getGumMetadata)) {
                assertNoErrorStatus(response);
                InputStream responseContent = response.getEntity().getContent();
                // Retrieve only first number of the version which correspond to the major version.
                return Integer.valueOf(extractStringFieldValueFromJson("version", responseContent).split("\\.")[0]);
            }
        } catch (IOException e) {
            throw new GumConfigClientException("Could not get GUM version", e);
        }
    }


    private void setConfigurationJson(InputStream responseContent) throws IOException {
        JsonNode jsonNode = new ObjectMapper().readTree(responseContent);
        configurationsJson = jsonNode.toString();
    }

    private void assertNoErrorStatus(CloseableHttpResponse response) {
        if (response.getStatusLine().getStatusCode() >= HttpStatus.SC_BAD_REQUEST) {
            String message = response.getStatusLine().getStatusCode() + " " +
                    response.getStatusLine().getReasonPhrase();
            try {
                String body = EntityUtils.toString(response.getEntity());
                if (body != null)
                    throw new GumConfigClientException(message + " with body: " + body);
                throw new GumConfigClientException(message);
            } catch (IOException e) {
                throw new GumConfigClientException(message);
            }
        }
    }

    private boolean extractBooleanFieldValueFromJson(String fieldName, String json) {
        try {
            JsonNode jsonNode = new ObjectMapper().readTree(json);
            return jsonNode.get(fieldName).asBoolean();
        } catch (IOException e) {
            throw new GumConfigClientException(e);
        }
    }

    private String extractStringFieldValueFromJson(String fieldName, InputStream json) {
        try {
            JsonNode jsonNode = new ObjectMapper().readTree(json);
            return jsonNode.get(fieldName).asText();
        } catch (IOException e) {
            throw new GumConfigClientException(e);
        }
    }
}
