package net.ihe.gazelle.ssov7.registration.interlay.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.util.Collections;

import static org.junit.Assert.assertEquals;

public class CasKeycloakClientDTOTest {

    @Test
    public void constructWithSetterTest() {
        CasKeycloakClientDTO casKeycloakClientDTO = new CasKeycloakClientDTO();
        casKeycloakClientDTO.setClientId("id");
        casKeycloakClientDTO.setClientAuthenticatorType("authType");
        casKeycloakClientDTO.setName("name");
        casKeycloakClientDTO.setBaseUrl("http://baseurl.base");
        casKeycloakClientDTO.setDefaultClientScopes(Collections.singletonList("default"));
        casKeycloakClientDTO.setOptionalClientScopes(Collections.singletonList("optional"));
        casKeycloakClientDTO.setRedirectUris(Collections.singletonList("http://redirect.com"));
        casKeycloakClientDTO.setProtocolMappers(Collections.singletonList("protocol"));

        assertEquals("id", casKeycloakClientDTO.getClientId());
        assertEquals("cas",casKeycloakClientDTO.getProtocol());
        assertEquals("authType", casKeycloakClientDTO.getClientAuthenticatorType());
        assertEquals("name", casKeycloakClientDTO.getName());
        assertEquals("http://baseurl.base", casKeycloakClientDTO.getBaseUrl());
        assertEquals(Collections.singletonList("default"), casKeycloakClientDTO.getDefaultClientScopes());
        assertEquals(Collections.singletonList("optional"), casKeycloakClientDTO.getOptionalClientScopes());
        assertEquals(Collections.singletonList("http://redirect.com"), casKeycloakClientDTO.getRedirectUris());
        assertEquals(Collections.singletonList("protocol"), casKeycloakClientDTO.getProtocolMappers());
    }

    @Test
    public void equalsTest(){
        EqualsVerifier.forClass(CasKeycloakClientDTO.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .withRedefinedSuperclass().verify();
    }

}