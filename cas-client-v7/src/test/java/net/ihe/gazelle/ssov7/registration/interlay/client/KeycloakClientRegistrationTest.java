package net.ihe.gazelle.ssov7.registration.interlay.client;

import com.github.tomakehurst.wiremock.client.MappingBuilder;
import com.github.tomakehurst.wiremock.http.RequestMethod;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.matching.RequestPatternBuilder;
import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

public class KeycloakClientRegistrationTest {

    private static final int PORT_NUMBER = Integer.parseInt(System.getProperty("testMockPort"));
    private static final String accessTokenMock = "thisIsAMockedAccessToken";
    private KeycloakClientRegistrationService keycloakClientRegistration;
    private final MetadataServiceProvider metadataServiceProvider = new MetadataServiceProviderMock();

    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(PORT_NUMBER));
    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();
    private MappingBuilder postAccessTockenMappingBuilder;
    private MappingBuilder getClientsMappingBuilder;
    private String clientId;


    @Before
    public void setUp() {
        postAccessTockenMappingBuilder = post(KeycloakClientRegistrationService.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "application/json")
                        .withBody("{\"access_token\":\"" + accessTokenMock + "\"}")
                );
        clientId = metadataServiceProvider.getMetadata().getName() + "-" + metadataServiceProvider.getMetadata().getInstanceId();
        getClientsMappingBuilder = get(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(ok()
                        .withHeader("Content-Type", "application/json")
                        .withBody("[{\"id\":\"idTest\"," +
                                "\"clientId\":\"" + clientId + "\"}]")
                );
        wireMockRule.stubFor(postAccessTockenMappingBuilder);

        environmentVariables.set("GZL_SSO_ADMIN_USER", "admin");
        environmentVariables.set("GZL_SSO_ADMIN_PASSWORD", "admin");

        keycloakClientRegistration = new KeycloakClientRegistrationService("http://localhost:" + PORT_NUMBER, 500);
    }

    @Test
    public void getAccessToken() {
        assertNotNull(keycloakClientRegistration.getAccessToken());

        verify(postRequestedFor(urlEqualTo(KeycloakClientRegistrationService.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN))
                .withHeader("Content-Type", containing("x-www-form-urlencoded"))
                .withRequestBody(containing("username=" + System.getenv("GZL_SSO_ADMIN_USER") +
                        "&password=" + System.getenv("GZL_SSO_ADMIN_PASSWORD") + "&grant_type=password&client_id=admin-cli")));

        assertEquals(accessTokenMock, keycloakClientRegistration.getAccessToken());
    }

    @Test
    public void getAccessTokenRetried() {
        wireMockRule.stubFor(post(KeycloakClientRegistrationService.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(serverError())
                .inScenario("Retry token")
                .whenScenarioStateIs(STARTED)
                .willSetStateTo("Success")
        );

        wireMockRule.stubFor(postAccessTockenMappingBuilder
                .inScenario("Retry token")
                .whenScenarioStateIs("Success")
        );

        assertNotNull(keycloakClientRegistration.getAccessToken());

        verify(moreThanOrExactly(2), postRequestedFor(urlEqualTo(KeycloakClientRegistrationService.REALMS_MASTER_PROTOCOL_OPENID_CONNECT_TOKEN))
                .withHeader("Content-Type", containing("x-www-form-urlencoded"))
                .withRequestBody(containing("username=" + System.getenv("GZL_SSO_ADMIN_USER") +
                        "&password=" + System.getenv("GZL_SSO_ADMIN_PASSWORD") + "&grant_type=password&client_id=admin-cli")));

        assertEquals(accessTokenMock, keycloakClientRegistration.getAccessToken());
    }

    @Test
    public void registerClientWhenClientIdNotUnique() {
        wireMockRule.stubFor(getClientsMappingBuilder
        );
        wireMockRule.stubFor(put(KeycloakClientRegistrationService.CLIENTS_URL + "/idTest")
                .willReturn(ok()));

        keycloakClientRegistration.registerClient(clientId, "http://localhost:8080");
        verify(getRequestedFor(urlEqualTo(KeycloakClientRegistrationService.CLIENTS_URL))
                .withHeader("Authorization", containing(accessTokenMock)));

        verify(getRegisterClientRequestPatternBuilder(RequestMethod.PUT, "/admin/realms/gazelle/clients/idTest"));
    }

    @Test
    public void registerClientWhenClientIdNotUniqueRetried() {

        wireMockRule.stubFor(postAccessTockenMappingBuilder
                .inScenario("Retry request")
                .whenScenarioStateIs(STARTED)
        );
        wireMockRule.stubFor(getClientsMappingBuilder
                .inScenario("Retry request")
                .whenScenarioStateIs(STARTED)
        );
        wireMockRule.stubFor(put("/admin/realms/gazelle/clients/idTest")
                .willReturn(serverError())
                .inScenario("Retry request")
                .whenScenarioStateIs(STARTED)
                .willSetStateTo("Success"));

        wireMockRule.stubFor(put("/admin/realms/gazelle/clients/idTest")
                .willReturn(ok())
                .inScenario("Retry request")
                .whenScenarioStateIs("Success"));

        keycloakClientRegistration.registerClient(clientId, "http://localhost:8080");

        verify(getRequestedFor(urlEqualTo(KeycloakClientRegistrationService.CLIENTS_URL))
                .withHeader("Authorization", containing(accessTokenMock)));

        verify(moreThanOrExactly(2), putRequestedFor(urlEqualTo("/admin/realms/gazelle/clients/idTest"))
                .withHeader("Authorization", containing(accessTokenMock)));
    }

    @Test
    public void registerNewClient() {
        wireMockRule.stubFor(get(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(ok()
                        .withHeader("Content-Type", "application/json")
                        .withBody("[]"))
        );

        wireMockRule.stubFor(post(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(ok()));

        keycloakClientRegistration.registerClient(clientId, "http://localhost:8080/");

        verify(getRequestedFor(urlEqualTo(KeycloakClientRegistrationService.CLIENTS_URL))
                .withHeader("Authorization", containing(accessTokenMock))
        );

        verify(getRegisterClientRequestPatternBuilder(RequestMethod.POST, "/admin/realms/gazelle/clients")
        );

    }

    @Test(expected = SSOClientRegistrationException.class)
    public void registerClientWhenServerError() {
        wireMockRule.stubFor(get(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(ok()
                        .withHeader("Content-Type", "application/json")
                        .withBody("[]"))
        );
        wireMockRule.stubFor(post(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(aResponse().withStatus(503))
        );
        keycloakClientRegistration.registerClient(clientId, "http://localhost:8080");
    }

    @Test(expected = SSOClientRegistrationException.class)
    public void registerClientWhenRequestError() {
        wireMockRule.stubFor(get(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(ok()
                        .withHeader("Content-Type", "application/json")
                        .withBody("[]"))
        );
        wireMockRule.stubFor(post(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(aResponse().withStatus(403))
        );
        keycloakClientRegistration.registerClient(clientId, "http://localhost:8080");
    }

    @Test
    public void getClientIdRetried() {

        wireMockRule.stubFor(get(KeycloakClientRegistrationService.CLIENTS_URL)
                .willReturn(serverError()
                )
                .inScenario("Retry getClient")
                .whenScenarioStateIs(STARTED)
                .willSetStateTo("Success")
        );

        wireMockRule.stubFor(getClientsMappingBuilder
                .inScenario("Retry getClient")
                .whenScenarioStateIs("Success")
        );

        keycloakClientRegistration.getIdByClientId(clientId);
        verify(moreThanOrExactly(2), getRequestedFor(urlEqualTo(KeycloakClientRegistrationService.CLIENTS_URL))
                .withHeader("Authorization", containing(accessTokenMock)));
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerClientWithServiceMetadataNull() {
        keycloakClientRegistration.registerClient(null, "http://localhost:8080");
    }

    @Test(expected = IllegalArgumentException.class)
    public void registerClientWithApplicationUrlNull() {
        keycloakClientRegistration.registerClient(clientId, null);
    }

    private RequestPatternBuilder getRegisterClientRequestPatternBuilder(RequestMethod requestMethod, String url) {
        return new RequestPatternBuilder(requestMethod, (urlEqualTo(url)))
                .withHeader("Authorization", containing(accessTokenMock))
                .withRequestBody(matchingJsonPath("clientId", containing(clientId)))
                .withRequestBody(matchingJsonPath("protocol", containing("cas")))
                .withRequestBody(matchingJsonPath("baseUrl", containing("http://localhost:8080")))
                .withRequestBody(matchingJsonPath("clientAuthenticatorType", containing("client-secret")))
                .withRequestBody(matchingJsonPath("defaultClientScopes", containing("cas-client-scope")))
                .withRequestBody(matchingJsonPath("redirectUris", containing("http://localhost:8080/*")));
    }
}