package net.ihe.gazelle.ssov7.m2m.server.interlay.factory;

import net.ihe.gazelle.ssov7.authn.interlay.Authenticator;
import net.ihe.gazelle.ssov7.authn.interlay.AuthenticatorFactory;
import net.ihe.gazelle.ssov7.m2m.server.interlay.M2MAuthenticator;

public class M2MAuthenticatorFactory implements AuthenticatorFactory {
    @Override
    public Authenticator createAuthenticator() {
        return new M2MAuthenticator();
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
