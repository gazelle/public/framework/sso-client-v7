package net.ihe.gazelle.ssov7.m2m.client.interlay.cache;

import net.ihe.gazelle.ssov7.m2m.client.application.accesstoken.AccessTokenProvider;
import org.codehaus.jackson.map.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.DatatypeConverter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class CacheAccessTokenProvider implements AccessTokenProvider {

    private static final Logger LOG = LoggerFactory.getLogger(CacheAccessTokenProvider.class);

    private final AccessTokenProvider accessTokenProvider;
    private static final Map<List<String>, String> cache = new HashMap<>();
    private final ObjectMapper mapper = new ObjectMapper();

    public CacheAccessTokenProvider(AccessTokenProvider accessTokenProvider) {
        this.accessTokenProvider = accessTokenProvider;
    }

    @Override
    public String getAccessToken(String clientId, String clientSecret) {
        return getAccessToken(clientId,clientSecret, Collections.singletonList("noScope"));
    }

    @Override
    public String getAccessToken(String clientId, String clientSecret, List<String> scopes) {
        String token = cache.get(scopes);

        if (isNewTokenRequired(token)) {
            LOG.trace("Refreshing the access token");
            token = accessTokenProvider.getAccessToken(clientId, clientSecret, scopes);
            cache.put(scopes, token);
        }
        return token;
    }

    private boolean isNewTokenRequired(String token) {
        if (token != null) {
            // Check if the token is expired
            long expirationTime = extractExpirationTimeFromToken(token);
            long currentTime = System.currentTimeMillis() / 1000;
            return expirationTime < currentTime;
        }
        return true;
    }

    private long extractExpirationTimeFromToken(String token) {
        // Decode the jwt in parameters and return the expiration time
        String[] splitString = token.split("\\.");
        StringBuilder base64EncodedBody = new StringBuilder(splitString[1]);
        base64Padding(base64EncodedBody);

        String body = new String(DatatypeConverter.parseBase64Binary(base64EncodedBody.toString()), StandardCharsets.UTF_8);

        // Get the expiration time
        try {
            return mapper.readTree(body).get("exp").asLong();
        } catch (IOException e) {
            LOG.warn("Unable to extract expiration time from jwt", e);
            return 0;
        }
    }

    /**
     * This function add padding, by adding equals signs at the end of the base 64 String, if its number of bytes is
     * not divisible by 4
     * @see <a href="https://www.w3.org/TR/xmlschema-2/#base64Binary">
     *     https://www.w3.org/TR/xmlschema-2/#base64Binary</a>
     */
    private void base64Padding(StringBuilder base64EncodedBody) {
        int base64EncodedBodyByteLength = base64EncodedBody.toString().getBytes(StandardCharsets.UTF_8).length;
        for (int i = 0; i < base64EncodedBodyByteLength % 4; i++) {
            base64EncodedBody.append("=");
        }
    }

}
