package net.ihe.gazelle.ssov7.m2m.common.interlay;

import net.ihe.gazelle.metadata.application.MetadataServiceProvider;
import net.ihe.gazelle.metadata.domain.Service;
import net.ihe.gazelle.metadata.interlay.MetadataServiceProviderImpl;
import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;

import javax.annotation.PostConstruct;
@AutoCreate
@Scope(ScopeType.APPLICATION)
@Name("m2mKeycloakConfigurationServiceImpl")
public class M2MKeycloakConfigurationServiceImpl implements M2MConfigurationService {

    private MetadataServiceProvider metadataServiceProvider;

    public M2MKeycloakConfigurationServiceImpl() {
        metadataServiceProvider = new MetadataServiceProviderImpl();
    }

    @PostConstruct
    public void init(){
        metadataServiceProvider = new MetadataServiceProviderImpl();
    }

    @Override
    public String getM2MClientId() {
        return "m2m-"+getClientId();
    }

    @Override
    public String getSSOServerBaseUrl() {
        return System.getenv("GZL_SSO_URL");
    }

    private String getClientId() {
        Service service = metadataServiceProvider.getMetadata();
        // Retrieve instanceId
        String instanceId = service.getInstanceId();
        // Retrieve serviceName
        String serviceName = service.getName();
        return serviceName + (instanceId == null ? "" : "-" + instanceId);
    }

}

