package net.ihe.gazelle.ssov7.m2m.common.domain;

import net.ihe.gazelle.ssov7.configuration.domain.SSOConfigurationService;

public interface M2MConfigurationService extends SSOConfigurationService {

    /**
     * Retrieve the client id of the SSO M2M client
     * @return the client id of the SSO M2M client
     */
    String getM2MClientId();

}
