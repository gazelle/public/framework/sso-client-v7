package net.ihe.gazelle.ssov7.m2m.common.interlay;

import net.ihe.gazelle.ssov7.m2m.common.interlay.dto.M2mClientScopeDTO;
import net.ihe.gazelle.ssov7.m2m.common.interlay.dto.OidcKeycloakClientDTO;
import net.ihe.gazelle.ssov7.m2m.common.interlay.dto.ProtocolMapperDTO;
import net.ihe.gazelle.ssov7.m2m.common.interlay.dto.RoleDTO;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationException;
import org.codehaus.jackson.map.ObjectMapper;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import java.io.IOException;
import java.util.*;

public class M2MKeycloakDTOMapper {
    private static final String REALM = "gazelle";
    public static final String FALSE = "false";
    private final ObjectMapper mapper;

    public M2MKeycloakDTOMapper() {
        mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonSerialize.Inclusion.NON_EMPTY);
    }

    public String createRealmRoleRepresentationJson(String roleId, String roleName) {
        if (roleName == null)
            throw new IllegalArgumentException("roleName is null");

        RoleDTO roleDTO = new RoleDTO();
        if (roleId != null && !roleId.isEmpty())
            roleDTO.setId(roleId);
        roleDTO.setName(roleName);
        roleDTO.setContainerId(REALM);
        roleDTO.setClientRole(false);

        try {
            return mapper.writeValueAsString(roleDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }

    }

    public String createRealmRoleRepresentationJson(String roleName) {
        return createRealmRoleRepresentationJson("", roleName);
    }

    public String createM2mClientScopeRepresentationJson(String clientScopeName, String description) {
        if (clientScopeName == null)
            throw new IllegalArgumentException("clientScopeName is null");
        if (description == null)
            throw new IllegalArgumentException("description is null");
        M2mClientScopeDTO clientScopeDTO = new M2mClientScopeDTO();
        clientScopeDTO.setName(clientScopeName);
        clientScopeDTO.setDescription(description);
        try {
            return mapper.writeValueAsString(clientScopeDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }
    }

    public String createProtocolMapperAudienceRepresentationJson(String audience) {
        return createProtocolMapperAudienceRepresentationJson(audience, "");
    }

    public String createProtocolMapperAudienceRepresentationJson(String audience, String protocolMapperId) {
        if (audience == null)
            throw new IllegalArgumentException("audience is null");


        ProtocolMapperDTO protocolMapperDTO = new ProtocolMapperDTO();
        if (protocolMapperId != null && !protocolMapperId.isEmpty())
            protocolMapperDTO.setId(protocolMapperId);
        protocolMapperDTO.setName("audience");
        protocolMapperDTO.setProtocol("openid-connect");
        protocolMapperDTO.setProtocolMapper("oidc-audience-mapper");
        Map<String, String> config = new HashMap<>();
        config.put("id.token.claim", FALSE);
        config.put("access.token.claim", "true");
        config.put("included.custom.audience", audience);
        config.put("userinfo.token.claim", FALSE);
        protocolMapperDTO.setConfig(config);

        try {
            return mapper.writeValueAsString(protocolMapperDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }
    }

    public String createProtocolMapperAuthzMethodRepresentationJson() {
        return createProtocolMapperAuthzMethodRepresentationJson("");
    }

    public String createProtocolMapperAuthzMethodRepresentationJson(String protocolMapperId) {
        ProtocolMapperDTO protocolMapperDTO = new ProtocolMapperDTO();
        if (protocolMapperId != null && !protocolMapperId.isEmpty())
            protocolMapperDTO.setId(protocolMapperId);
        protocolMapperDTO.setName("authz_method");
        protocolMapperDTO.setProtocol("openid-connect");
        protocolMapperDTO.setProtocolMapper("oidc-hardcoded-claim-mapper");
        Map<String, String> config = new HashMap<>();
        config.put("id.token.claim", FALSE);
        config.put("access.token.claim", "true");
        config.put("claim.value", "M2M");
        config.put("claim.name", "authz_method");
        config.put("userinfo.token.claim", FALSE);
        protocolMapperDTO.setConfig(config);

        try {
            return mapper.writeValueAsString(protocolMapperDTO);
        } catch (IOException e) {
            throw new M2MKeycloakDTOMapperException(e);
        }
    }

    public String createClientRepresentationJson(String clientId) {
        if (clientId == null)
            throw new IllegalArgumentException("clientId is null");

        OidcKeycloakClientDTO clientRepresentation = new OidcKeycloakClientDTO();
        clientRepresentation.setAuthorizationServicesEnabled(false);
        clientRepresentation.setClientAuthenticatorType("client-secret");
        clientRepresentation.setClientId(clientId);
        clientRepresentation.setDirectAccessGrantsEnabled(false);
        clientRepresentation.setName(clientId);
        //this scope is create at Keycloak deployment
        List<String> defaultClientScopes = new ArrayList<>();
        defaultClientScopes.add("m2m-client-scope");
        defaultClientScopes.add("microprofile-jwt");
        clientRepresentation.setDefaultClientScopes(defaultClientScopes);
        clientRepresentation.setPublicClient(false);
        clientRepresentation.setSecret(System.getenv("GZL_M2M_CLIENT_SECRET"));
        clientRepresentation.setServiceAccountsEnabled(true);
        clientRepresentation.setStandardFlowEnabled(false);

        try {
            return mapper.writeValueAsString(clientRepresentation);
        } catch (IOException e) {
            throw new SSOClientRegistrationException(e);
        }
    }
}
