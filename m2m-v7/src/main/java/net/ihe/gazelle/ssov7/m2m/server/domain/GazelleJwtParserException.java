package net.ihe.gazelle.ssov7.m2m.server.domain;

public class GazelleJwtParserException extends RuntimeException {

    public GazelleJwtParserException() {
    }

    public GazelleJwtParserException(String s) {
        super(s);
    }

    public GazelleJwtParserException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public GazelleJwtParserException(Throwable throwable) {
        super(throwable);
    }
}
