package net.ihe.gazelle.ssov7.m2m.client.interlay.dao;

import net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientDAOException;
import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import net.ihe.gazelle.ssov7.m2m.client.application.registration.M2MClientDAO;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakDTOMapper;
import net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient;
import net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClientException;
import net.ihe.gazelle.ssov7.registration.domain.SSOClientRegistrationException;
import org.apache.http.HttpHeaders;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URIBuilder;
import org.apache.http.impl.client.CloseableHttpClient;
import org.jboss.seam.ScopeType;
import org.jboss.seam.annotations.AutoCreate;
import org.jboss.seam.annotations.In;
import org.jboss.seam.annotations.Name;
import org.jboss.seam.annotations.Scope;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;

import static net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClient.*;


@AutoCreate
@Name("m2MClientDAOImpl")
@Scope(ScopeType.APPLICATION)
public class M2MClientDAOImpl implements M2MClientDAO {

    private static final Logger LOG = LoggerFactory.getLogger(M2MClientDAOImpl.class);
    public static final String CLIENTS = "/clients/";


    @In(value = "m2mKeycloakConfigurationServiceImpl")
    private M2MConfigurationService m2MConfigurationService;

    KeycloakHttpClient keycloakHttpClient;

    M2MKeycloakDTOMapper keycloakDTOMapper;
    private String keycloakUrl;

    public M2MClientDAOImpl() {
        //mandatory empty constructor for Injection
    }

    public M2MClientDAOImpl(M2MConfigurationService m2MConfigurationService) {
        this.m2MConfigurationService = m2MConfigurationService;
        keycloakUrl = this.m2MConfigurationService.getSSOServerBaseUrl();
        keycloakDTOMapper = new M2MKeycloakDTOMapper();
        keycloakHttpClient = new KeycloakHttpClient(keycloakUrl);
    }

    @PostConstruct
    public void init() {
        keycloakUrl = m2MConfigurationService.getSSOServerBaseUrl();
        keycloakDTOMapper = new M2MKeycloakDTOMapper();
        keycloakHttpClient = new KeycloakHttpClient(keycloakUrl);

    }

    @Override
    public String getClientScopeId(String clientScopeName) {
        String clientScopeUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENT_SCOPES;
        return keycloakHttpClient.getIdBySingleQueryParam("name", clientScopeName, clientScopeUrl);
    }

    @Override
    public String getRealmRoleId(String roleName) {
        String roleUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + "/roles";
        return keycloakHttpClient.getIdBySingleQueryParam("name", roleName, roleUrl);
    }

    @Override
    public void addClientScopeToClient(String idOfClient, String clientScopeId, boolean optional) {
        String kindOfClientScope = optional ? "optional-client-scopes" : "default-client-scopes";
        String clientScopeUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENTS + idOfClient + "/" +kindOfClientScope+ "/" + clientScopeId;

        HttpPut httpPut = new HttpPut();
        try (CloseableHttpClient httpClient = keycloakHttpClient.buildCloseableHttpClient()) {

            URI uri = new URIBuilder(clientScopeUrl).build();
            httpPut.setURI(uri);
            httpPut.addHeader(HttpHeaders.AUTHORIZATION, BEARER + keycloakHttpClient.getAccessToken());

            try (CloseableHttpResponse response = httpClient.execute(httpPut)) {
                keycloakHttpClient.assertNoErrorStatus(response);

                LOG.trace("Added optional client scope successfully");
            }
        } catch (IOException | URISyntaxException | KeycloakHttpClientException e) {
            throw new SSOClientRegistrationException("Failed to add client scope to client " + idOfClient, e);
        }
    }

    @Override
    public String getIdOfClient(String clientId) {
        String clientUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + "/clients";
        return keycloakHttpClient.getIdBySingleQueryParam("clientId", clientId, clientUrl);
    }

    @Override
    public void createClient(String clientId) {
        String clientUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + "/clients";
        try {
            keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(clientUrl),
                    keycloakDTOMapper.createClientRepresentationJson(clientId));
        } catch (KeycloakHttpClientException e) {
            throw new M2MClientDAOException("Failed to create client" + clientId, e);
        }
    }

    @Override
    public void updateClient(String idOfClient, String clientId) {
        String clientUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENTS + idOfClient;
        try {
            keycloakHttpClient.executeRequestWithJsonBody(new HttpPut(clientUrl),
                    keycloakDTOMapper.createClientRepresentationJson(clientId));
        } catch (KeycloakHttpClientException e) {
            throw new M2MClientDAOException("Failed to update client" + clientId, e);
        }
    }

    @Override
    public void grantRoleToClient(String idOfClient, String roleId, String roleName) {
        String serviceAccountUrl = keycloakUrl + ADMIN_REALMS_GAZELLE + CLIENTS + idOfClient + "/service-account-user";
        String serviceAccountId = keycloakHttpClient.getIdBySingleQueryParam("", "", serviceAccountUrl);
        String grantRoleUrl = keycloakUrl + "/admin/realms/gazelle/users/" + serviceAccountId + "/role-mappings/realm";

        try {
        keycloakHttpClient.executeRequestWithJsonBody(new HttpPost(grantRoleUrl),
                "[" + keycloakDTOMapper.createRealmRoleRepresentationJson(roleId, roleName) + "]");
        } catch (KeycloakHttpClientException e) {
            throw new M2MClientDAOException("Failed to grant role "+roleName+" to client" + idOfClient,e);
        }
    }
}