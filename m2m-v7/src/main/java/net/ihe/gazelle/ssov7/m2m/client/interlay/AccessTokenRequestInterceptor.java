package net.ihe.gazelle.ssov7.m2m.client.interlay;


import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import net.ihe.gazelle.ssov7.m2m.client.application.accesstoken.AccessTokenProvider;
import net.ihe.gazelle.ssov7.m2m.client.interlay.cache.CacheAccessTokenProvider;
import net.ihe.gazelle.ssov7.m2m.client.interlay.client.KeycloakAccessTokenProvider;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import org.apache.http.HttpHeaders;
import org.apache.http.HttpRequest;
import org.apache.http.HttpRequestInterceptor;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HttpContext;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

public class AccessTokenRequestInterceptor implements HttpRequestInterceptor {

    private final M2MConfigurationService m2MConfigurationService;
    private final AccessTokenProvider accessTokenProvider;

    private List<String> scopes;
    private final String clientSecret;

    public AccessTokenRequestInterceptor() {
        this(null);
    }

    public AccessTokenRequestInterceptor(List<String> scopes) {
        if(scopes != null)
            this.scopes = new ArrayList<>(scopes);
        m2MConfigurationService = new M2MKeycloakConfigurationServiceImpl();
        accessTokenProvider = new CacheAccessTokenProvider(new KeycloakAccessTokenProvider(m2MConfigurationService));
        clientSecret = Objects.requireNonNull(
                System.getenv("GZL_M2M_CLIENT_SECRET"),
                "GZL_M2M_CLIENT_SECRET should not be null"
        );
    }


    @Override
    public void process(HttpRequest request, HttpContext context) {
        request.addHeader(
                new BasicHeader(
                        HttpHeaders.AUTHORIZATION, "Bearer " +
                        accessTokenProvider.getAccessToken(m2MConfigurationService.getM2MClientId(), clientSecret, scopes)
                )
        );
    }
}