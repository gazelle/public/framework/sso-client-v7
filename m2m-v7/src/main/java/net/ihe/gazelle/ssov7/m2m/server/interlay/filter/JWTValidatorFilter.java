package net.ihe.gazelle.ssov7.m2m.server.interlay.filter;


import net.ihe.gazelle.ssov7.m2m.server.application.jwt.JWTValidator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;


public class JWTValidatorFilter implements Filter {
    private static final Logger LOG = LoggerFactory.getLogger(JWTValidatorFilter.class);
    public static final String SESSION_ATTRIBUTE_JWT = "jwt";
    public static final String RFC_6750_BEARER = "Bearer";


    @Override
    public void init(FilterConfig filterConfig) throws ServletException {
        // Nothing to do here
    }


    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        LOG.trace("In JWT filter");

        HttpServletRequest httpServletRequest = (HttpServletRequest) servletRequest;
        JWTValidator jwtValidator = new JWTValidator();
        String authzHeader = httpServletRequest.getHeader("Authorization");

        if (authzHeader != null && authzHeader.contains(RFC_6750_BEARER)) {
            LOG.trace("Validating JWT");
            HttpSession session = httpServletRequest.getSession();
            try {
                String tokenFromAuthzHeader = getTokenFromAuthzHeader(authzHeader);
                if (tokenFromAuthzHeader.isEmpty())
                    filterChain.doFilter(servletRequest,servletResponse);
                jwtValidator.assertValid(tokenFromAuthzHeader);
                LOG.trace("JWT is validated");
                session.setAttribute(SESSION_ATTRIBUTE_JWT, tokenFromAuthzHeader);
                filterChain.doFilter(servletRequest, servletResponse);
            } catch (Exception e) {
                LOG.error("401 Unauthorized error", e);
                session.setAttribute("m2m_principal", null);
                HttpServletResponse httpServletResponse = (HttpServletResponse) servletResponse;
                httpServletResponse.sendError(401, "Unauthorized");
            }
        } else {
            filterChain.doFilter(servletRequest, servletResponse);
        }
    }

    private String getTokenFromAuthzHeader(String authzHeader) {
        return authzHeader.replace(RFC_6750_BEARER, "").trim();
    }

    @Override
    public void destroy() {
        // Nothing to do here
    }
}
