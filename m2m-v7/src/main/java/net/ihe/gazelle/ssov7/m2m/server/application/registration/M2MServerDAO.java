package net.ihe.gazelle.ssov7.m2m.server.application.registration;

public interface M2MServerDAO {
    String getClientScopeId(String name);

    void updateClientScope(String clientScopeId, String name, String description);

    void createClientScope(String name, String description);

    String getRealmRoleId(String roleName);

    boolean scopeMappingOfRoleExists(String clientScopeId, String role);

    void createScopeMappingOfRole(String clientScopeId, String role, String machineRole);

    void updateProtocolMapperAudience(String protocolMapperAudienceId, String clientScopeId, String audience);

    String getProtocolMapperId(String clientScopeId, String audience);

    void createProtocolMapperAudience(String clientScopeId, String clientId);

    void updateProtocolMapperAuthzMethod(String protocolMapperAuthzMethodId, String clientScopeId);

    void createProtocolMapperAuthzMethod(String clientScopeId);
}
