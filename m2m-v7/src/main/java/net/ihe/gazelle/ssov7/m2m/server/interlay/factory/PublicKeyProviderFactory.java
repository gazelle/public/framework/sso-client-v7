package net.ihe.gazelle.ssov7.m2m.server.interlay.factory;


import net.ihe.gazelle.ssov7.m2m.server.application.publickey.PublicKeyConfig;
import net.ihe.gazelle.ssov7.m2m.server.application.publickey.SSOPublicKeyProvider;
import net.ihe.gazelle.ssov7.m2m.server.interlay.provider.KeyStorePKProvider;
import net.ihe.gazelle.ssov7.m2m.server.interlay.provider.KeycloakPKProvider;
import net.ihe.gazelle.ssov7.m2m.server.interlay.provider.LocalPKProvider;

public class PublicKeyProviderFactory {

    private final PublicKeyConfig publicKeyConfig;

    public PublicKeyProviderFactory(PublicKeyConfig publicKeyConfig) {
        this.publicKeyConfig = publicKeyConfig;
    }

    public SSOPublicKeyProvider getPublicKeyProvider() {
        String keystoreAlias = publicKeyConfig.getKeystoreAlias();
        String keystorePassword = publicKeyConfig.getKeystorePassword();
        String publicKeyPath = publicKeyConfig.getPublicKeyPath();
        String keyId = publicKeyConfig.getKeyId();
        if (keystoreAlias != null && keystorePassword != null) {
            return new KeyStorePKProvider(keystoreAlias, keystorePassword, publicKeyPath);
        } else if (publicKeyPath.startsWith("http")) {
            return new KeycloakPKProvider(publicKeyPath, keyId);
        } else {
            return new LocalPKProvider(publicKeyPath);
        }
    }
}
