package net.ihe.gazelle.ssov7.m2m.server.application.publickey;

/**
 * This interface is used to provide the necessary configuration to retrieve a public Key
 */
public interface PublicKeyConfig {

    /**
     * The path of the public key that will be used to validate the JWT signature
     * It can be a local path to a .pem file, a URL or a path to .jks keystore
     *
     * @return the public key path
     */
    String getPublicKeyPath();

    /**
     * The alias that will be used to retrieve the public from the keystore
     *
     * @return the alias of the key
     */
    String getKeystoreAlias();

    /**
     * The password that is necessary to access the keystore
     *
     * @return the password of the keystore
     */
    String getKeystorePassword();

    /**
     * The id of the public key to retrieve when extracting the key from an array of JSON Web Keys
     *
     * @return the id of the public key
     */
    String getKeyId();
}
