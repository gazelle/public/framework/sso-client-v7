package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;

import java.util.Map;

public class M2mClientScopeDTO extends ClientScopeDTO{

    public M2mClientScopeDTO(){
        super("openid-connect");
        addAttribute("include.in.token.scope","true");
        addAttribute("display.on.consent.screen","false");
    }

    @Override
    public void setAttributes(Map<String, String> attributes) {
        super.setAttributes(attributes);
        addAttribute("include.in.token.scope","true");
        addAttribute("display.on.consent.screen","false");
    }
}
