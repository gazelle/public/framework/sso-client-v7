package net.ihe.gazelle.ssov7.m2m.client.application.registration;

public class M2MClientDAOException extends RuntimeException {

    public M2MClientDAOException() {
        super();
    }

    public M2MClientDAOException(String s) {
        super(s);
    }

    public M2MClientDAOException(String s, Throwable throwable) {
        super(s, throwable);
    }
}


