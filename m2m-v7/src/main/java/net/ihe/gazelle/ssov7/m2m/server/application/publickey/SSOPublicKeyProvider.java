package net.ihe.gazelle.ssov7.m2m.server.application.publickey;

import java.security.PublicKey;

/**
 * This class is used to retrieve the public key to match against the JWT signature
 */
public interface SSOPublicKeyProvider {

    /**
     * Method to get public key
     *
     * @return the PublicKey
     */
    PublicKey getPublicKey();
}
