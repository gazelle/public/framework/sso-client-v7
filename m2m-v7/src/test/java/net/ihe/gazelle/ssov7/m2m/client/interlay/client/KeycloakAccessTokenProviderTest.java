package net.ihe.gazelle.ssov7.m2m.client.interlay.client;

import com.github.tomakehurst.wiremock.junit.WireMockRule;
import net.ihe.gazelle.ssov7.m2m.client.application.accesstoken.AccessTokenProvider;
import net.ihe.gazelle.ssov7.m2m.common.domain.M2MConfigurationService;
import net.ihe.gazelle.ssov7.m2m.common.interlay.M2MKeycloakConfigurationServiceImpl;
import net.ihe.gazelle.ssov7.m2m.common.interlay.client.KeycloakHttpClientException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.mockito.junit.MockitoJUnitRunner;

import java.util.Collections;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.options;
import static com.github.tomakehurst.wiremock.stubbing.Scenario.STARTED;
import static net.ihe.gazelle.ssov7.m2m.client.interlay.client.KeycloakAccessTokenProvider.REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

@RunWith(MockitoJUnitRunner.class)
public class KeycloakAccessTokenProviderTest {

    private AccessTokenProvider accessTokenProvider;

    private final Integer port = Integer.valueOf(System.getProperty("testMockPort"));
    @Rule
    public WireMockRule wireMockRule = new WireMockRule(options().port(port));

    private static final String clientId = "clientId";
    private static final String clientSecret = "clientSecret";

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        M2MConfigurationService m2MConfigurationService = mock(M2MKeycloakConfigurationServiceImpl.class);
        when(m2MConfigurationService.getSSOServerBaseUrl()).thenReturn("http://localhost:" + port);
        accessTokenProvider = new KeycloakAccessTokenProvider(m2MConfigurationService);
    }

    @Test
    public void getAccessToken() {
        String expectedAccessToken = "thisIsAMockedAccessToken";
        wireMockRule.stubFor(post(REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(ok()
                        .withHeader("Content-type", "net/ihe/gazelle/application/json")
                        .withBody("{\"access_token\":\"" + expectedAccessToken + "\"}")
                ));

        String accessTokenReceived = accessTokenProvider.getAccessToken(clientId, clientSecret, Collections.<String>emptyList());
        assertNotNull(accessTokenReceived);

        verify(postRequestedFor(urlEqualTo(REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN))
                .withHeader("Content-Type", containing("x-www-form-urlencoded"))
                .withRequestBody(
                        containing(
                                "grant_type=" + KeycloakAccessTokenProvider.CLIENT_CREDENTIALS
                                        + "&client_id=" + clientId
                                        + "&client_secret=" + clientSecret)
                )
        );

        assertEquals(expectedAccessToken, accessTokenReceived);
    }

    @Test
    public void getAccessTokenWithRetry() {
        String expectedAccessToken = "thisIsAMockedAccessToken";
        String scopeTest = "scope:test";
        String retryBecauseInvalidScope = "Retry because invalid scope";
        String gazelleClientsAdminToken = "gazelle clients admin token";
        wireMockRule.stubFor(post(REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN)
                .inScenario(retryBecauseInvalidScope)
                .whenScenarioStateIs(STARTED)
                .willReturn(badRequest().withBody(
                                "{\"error\":\"" + KeycloakAccessTokenProvider.INVALID_SCOPE_ERROR
                                        + "\"," +
                                        "\"error_description\":\"Invalid scopes: " + scopeTest + "\"}"
                        )
                ).willSetStateTo(gazelleClientsAdminToken)
        );

        String invalidScope = "invalid scope";
        wireMockRule.stubFor(post(REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN)
                .inScenario(retryBecauseInvalidScope)
                .whenScenarioStateIs(gazelleClientsAdminToken)
                .willReturn(ok()
                        .withBody("{\"access_token\":\"" + expectedAccessToken + "\"}")
                )
                .willSetStateTo(invalidScope)
        );

        String accessTokenReceived = accessTokenProvider.getAccessToken(clientId, clientSecret,
                Collections.singletonList(scopeTest));
        assertNotNull(accessTokenReceived);

        verify(2, postRequestedFor(urlEqualTo(REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN))
                .withHeader("Content-Type", containing("x-www-form-urlencoded"))
                .withRequestBody(
                        containing(
                                "grant_type=" + KeycloakAccessTokenProvider.CLIENT_CREDENTIALS
                                        + "&client_id=" + clientId
                                        + "&client_secret=" + clientSecret)
                )
        );
        assertEquals(expectedAccessToken, accessTokenReceived);
    }

    @Test(expected = KeycloakHttpClientException.class)
    public void getAccessTokenError() {
        wireMockRule.stubFor(post(REALMS_GAZELLE_PROTOCOL_OPENID_CONNECT_TOKEN)
                .willReturn(aResponse().withStatus(403)));
        accessTokenProvider.getAccessToken(clientId, clientSecret, Collections.<String>emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAccessTokenWithoutClientId() {
        accessTokenProvider.getAccessToken(null, clientSecret, Collections.<String>emptyList());
    }

    @Test(expected = IllegalArgumentException.class)
    public void getAccessTokenWithoutClientSecret() {
        accessTokenProvider.getAccessToken(clientId, null, Collections.<String>emptyList());
    }
}