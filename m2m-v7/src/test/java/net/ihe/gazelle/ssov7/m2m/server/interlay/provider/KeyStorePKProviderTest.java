package net.ihe.gazelle.ssov7.m2m.server.interlay.provider;

import org.junit.Test;

import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.*;
import java.security.cert.Certificate;

import static org.junit.Assert.assertEquals;

public class KeyStorePKProviderTest {

    @Test
    public void getPublicKeyFromKeystore() throws KeyStoreException {
        String path = "src/test/resources/server.keystore";
        String password = "changeit";
        String alias = "providerTest";
        KeyStorePKProvider ssoPublicKeyProvider = new KeyStorePKProvider(
                alias,
                password,
                path);
        PublicKey expectedKey = getKeyPairFromKeystore(path, alias, password).getPublic();
        assertEquals(expectedKey, ssoPublicKeyProvider.getPublicKey());
    }

    private KeyPair getKeyPairFromKeystore(String path, String alias, String password) throws KeyStoreException {
        KeyStore keyStore = KeyStore.getInstance("jks");
        try (InputStream inputStream = Files.newInputStream(Paths.get(path))) {

            keyStore.load(inputStream, password.toCharArray());

            Key key = keyStore.getKey(alias, password.toCharArray());
            if (key instanceof PrivateKey) {
                // Get certificate of public key
                Certificate cert = keyStore.getCertificate(alias);

                // Get public key
                PublicKey publicKey = cert.getPublicKey();

                // Return a key pair
                return new KeyPair(publicKey, (PrivateKey) key);
            }
            return null;
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }


}