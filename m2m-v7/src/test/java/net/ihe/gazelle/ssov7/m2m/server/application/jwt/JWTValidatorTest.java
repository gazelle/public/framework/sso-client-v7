package net.ihe.gazelle.ssov7.m2m.server.application.jwt;

import io.jsonwebtoken.Jwts;
import net.ihe.gazelle.ssov7.m2m.server.utils.PublicKeyGenerator;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.EnvironmentVariables;

import java.io.File;
import java.security.KeyPair;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import static org.junit.Assert.fail;
import static org.testng.AssertJUnit.assertTrue;

public class JWTValidatorTest {

    private static final PublicKeyGenerator publicKeyGenerator = new PublicKeyGenerator();
    JWTValidator jwtValidator;

    @Rule
    public final EnvironmentVariables environmentVariables = new EnvironmentVariables();

    private static final String ISSUER = "issuer";
    private static final String AUDIENCE = "http://localhost";

    @Before
    public void setUp() {
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", "expectedKey.pem");
    }

    @Test
    public void validateSignWithLocalKey() {
        environmentVariables.set("JWT_VERIFY_ISSUER", ISSUER);
        environmentVariables.set("ROOT_TEST_BED_URL", AUDIENCE);
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString("expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(keyPair.getPublic()));
        jwtValidator = new JWTValidator();

        try {
            Date dateNow = new GregorianCalendar().getTime();

            Calendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(calendar.getTimeInMillis() + 60000); //set time to now plus 60 seconds

            String jwt = Jwts.builder()
                    .issuer(ISSUER)
                    .issuedAt(dateNow)
                    .expiration(calendar.getTime())
                    .audience().add(AUDIENCE).and()
                    .signWith(keyPair.getPrivate())
                    .compact();

            jwtValidator.assertValid(jwt);
            assertTrue("assertValid(jwt) must not throw any exception", true);
        } catch (Exception e) {
            fail(e.getMessage());
        }
    }

    @Test(expected = IllegalArgumentException.class)
    public void validateSignWithJWTNull() {
        environmentVariables.set("JWT_VERIFY_ISSUER", ISSUER);
        environmentVariables.set("ROOT_TEST_BED_URL", AUDIENCE);
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString("expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(keyPair.getPublic()));
        jwtValidator = new JWTValidator();
        jwtValidator.assertValid(null);
    }


    @Test(expected = JWTValidatorException.class)
    public void validateSignWithBadLocalKey() {
        environmentVariables.set("JWT_VERIFY_ISSUER", ISSUER);
        environmentVariables.set("ROOT_TEST_BED_URL", AUDIENCE);
        environmentVariables.set("JWT_VERIFY_PUBLIC_KEY_LOCATION", "unexpectedKey.pem");

        KeyPair unexpectedKeyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString(
                "unexpectedKey.pem",
                publicKeyGenerator.getPublicKeyString(unexpectedKeyPair.getPublic())
        );
        jwtValidator = new JWTValidator();

        KeyPair expectedKeyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString(
                "expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(expectedKeyPair.getPublic())
        );

        String jwt = Jwts.builder()
                .issuer(ISSUER)
                .audience().add(AUDIENCE).and()
                .signWith(expectedKeyPair.getPrivate())
                .compact();

        jwtValidator.assertValid(jwt);
        fail("JWT should not be validated");
    }

    @Test(expected = JWTValidatorException.class)
    public void validateSignWithLocalKeyWithoutAudience() {
        environmentVariables.set("JWT_VERIFY_ISSUER", ISSUER);
        environmentVariables.set("ROOT_TEST_BED_URL", AUDIENCE);
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString("expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(keyPair.getPublic()));
        jwtValidator = new JWTValidator();
        String jwt = Jwts.builder()
                .issuer(ISSUER)
                .signWith(keyPair.getPrivate())
                .compact();
        jwtValidator.assertValid(jwt);
        fail("JWT should not be validated");
    }

    @Test(expected = JWTValidatorException.class)
    public void validateSignWithLocalKeyWithoutIssuer() {
        environmentVariables.set("JWT_VERIFY_ISSUER", ISSUER);
        environmentVariables.set("ROOT_TEST_BED_URL", AUDIENCE);
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString("expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(keyPair.getPublic()));
        jwtValidator = new JWTValidator();
        String jwt = Jwts.builder()
                .audience().add(AUDIENCE).and()
                .signWith(keyPair.getPrivate())
                .compact();
        jwtValidator.assertValid(jwt);
        fail("JWT should not be validated");
    }

    @Test(expected = JWTValidatorException.class)
    public void validateJWTExpired() {
        environmentVariables.set("JWT_VERIFY_ISSUER", ISSUER);
        environmentVariables.set("ROOT_TEST_BED_URL", AUDIENCE);
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString("expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(keyPair.getPublic()));
        jwtValidator = new JWTValidator();
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(calendar.getTimeInMillis() - 10000); //set time to now minus 10 seconds
        String jwt = Jwts.builder()
                .issuer(ISSUER)
                .expiration(calendar.getTime())
                .audience().add(AUDIENCE).and()
                .signWith(keyPair.getPrivate())
                .compact();
        jwtValidator.assertValid(jwt);
        fail("JWT should not be validated");
    }

    @Test(expected = JWTValidatorException.class)
    public void validateJWTBadCreationTime() {
        environmentVariables.set("JWT_VERIFY_ISSUER", ISSUER);
        environmentVariables.set("ROOT_TEST_BED_URL", AUDIENCE);
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyGenerator.generatePublicKeyFileFromString("expectedKey.pem",
                publicKeyGenerator.getPublicKeyString(keyPair.getPublic()));
        jwtValidator = new JWTValidator();
        Calendar calendar = new GregorianCalendar();
        calendar.setTimeInMillis(calendar.getTimeInMillis() + 60000); //set time to now plus 60 seconds
        String jwt = Jwts.builder()
                .issuer(ISSUER)
                .issuedAt(calendar.getTime())
                .audience().add(AUDIENCE).and()
                .signWith(keyPair.getPrivate())
                .compact();
        jwtValidator.assertValid(jwt);
        fail("JWT should not be validated");
    }


    @AfterClass
    public static void tearDownAll() {
        File expectedKeyFile = new File("./expectedKey.pem");
        expectedKeyFile.deleteOnExit();
        File badKeyFile = new File("./unexpectedKey.pem");
        badKeyFile.deleteOnExit();
    }
}