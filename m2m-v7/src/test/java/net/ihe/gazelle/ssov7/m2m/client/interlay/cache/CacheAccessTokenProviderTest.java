package net.ihe.gazelle.ssov7.m2m.client.interlay.cache;


import net.ihe.gazelle.ssov7.m2m.client.application.accesstoken.AccessTokenProvider;
import net.ihe.gazelle.ssov7.m2m.client.interlay.client.KeycloakAccessTokenProvider;
import org.junit.Before;
import org.junit.Test;
import org.mockito.MockitoAnnotations;

import javax.xml.bind.DatatypeConverter;
import java.nio.charset.StandardCharsets;
import java.util.Collections;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class CacheAccessTokenProviderTest {
    private AccessTokenProvider cacheAccessTokenProvider;
    private AccessTokenProvider keycloakAccessTokenProvider;
    private long currentTime;

    @Before
    public void setUp() {
        MockitoAnnotations.initMocks(this);
        keycloakAccessTokenProvider = mock(KeycloakAccessTokenProvider.class);
        cacheAccessTokenProvider = new CacheAccessTokenProvider(keycloakAccessTokenProvider);
        currentTime = System.currentTimeMillis() / 1000;
    }

    @Test
    public void cachedAccessTokenTest() {
        String clientId = "clientId";
        String clientSecret = "clientSecret";
        long expiration =  currentTime + 60;
        String mockedAccessToken = generateFakeJwt(expiration);
        List<String> scopes = Collections.singletonList("scope:test");
        when(keycloakAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes))
                .thenReturn(mockedAccessToken);
        String tokenFetched = cacheAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes);
        assertNotNull(tokenFetched);

        when(keycloakAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes))
                .thenReturn(null);
        String tokenCached = cacheAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes);
        assertNotNull(tokenCached);
        assertEquals(tokenFetched, tokenCached);
    }

    @Test
    public void expiredAccessTokenTest() {
        String clientId = "clientId";
        String clientSecret = "clientSecret";
        long expiredTime = currentTime - 60;
        String mockedAccessTokenExpired = generateFakeJwt(expiredTime);
        List<String> scopes = Collections.singletonList("scope:test");
        //simulate a cached JWT that has expired
        when(keycloakAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes))
                .thenReturn(mockedAccessTokenExpired);
        String expiredToken = cacheAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes);
        assertNotNull(expiredToken);

        //should fetch a new token
        long expiration = currentTime + 60;
        String mockedAccessToken = generateFakeJwt(expiration);
        when(keycloakAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes))
                .thenReturn(mockedAccessToken);
        String tokenCached = cacheAccessTokenProvider.getAccessToken(clientId, clientSecret, scopes);
        assertNotNull(tokenCached);
        assertEquals(mockedAccessToken, tokenCached);
    }

    private String generateFakeJwt(long expiration) {
        String fake = DatatypeConverter.printBase64Binary
                (("{}").getBytes(StandardCharsets.UTF_8)) +
                "." +
                DatatypeConverter.printBase64Binary
                        (("{\"exp\":\"" + expiration + "\"}").getBytes(StandardCharsets.UTF_8)) +
                "." +
                DatatypeConverter.printBase64Binary
                        (("{}").getBytes(StandardCharsets.UTF_8));
        return fake.replaceAll("=", "");
    }
}
