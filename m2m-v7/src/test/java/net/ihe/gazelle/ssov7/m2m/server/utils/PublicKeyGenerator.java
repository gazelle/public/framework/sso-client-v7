package net.ihe.gazelle.ssov7.m2m.server.utils;

import javax.xml.bind.DatatypeConverter;
import java.io.FileOutputStream;
import java.io.IOException;
import java.security.KeyPair;
import java.security.KeyPairGenerator;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;

public class PublicKeyGenerator {


    public String getPublicKeyString(PublicKey publicKey) {

        return "-----BEGIN PUBLIC KEY-----" +
                DatatypeConverter.printBase64Binary(publicKey.getEncoded())
                + "-----END PUBLIC KEY-----";
    }

    public KeyPair generateKeyPair() {
        try {
            KeyPairGenerator generator = KeyPairGenerator.getInstance("RSA");
            generator.initialize(2048);
            return generator.generateKeyPair();
        } catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public void generatePublicKeyFileFromString(String fileName,String publicKeyString) {
        try (FileOutputStream fos = new FileOutputStream(fileName)) {
            fos.write(publicKeyString.getBytes());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }
}
