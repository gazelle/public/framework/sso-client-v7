package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;

public class RoleDTOTest {

    @Test
    public void constructWithSettersTest(){
        RoleDTO roleDTO = new RoleDTO();
        roleDTO.setClientRole(false);
        roleDTO.setId("id");
        roleDTO.setName("role");
        roleDTO.setContainerId("gazelle");

        assertEquals("id",roleDTO.getId());
        assertFalse(roleDTO.isClientRole());
        assertEquals("role",roleDTO.getName());
        assertEquals("gazelle", roleDTO.getContainerId());
    }

    @Test
    public void equalsTest(){
        EqualsVerifier.forClass(RoleDTO.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
    }

}