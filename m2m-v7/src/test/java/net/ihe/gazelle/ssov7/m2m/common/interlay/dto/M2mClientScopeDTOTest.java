package net.ihe.gazelle.ssov7.m2m.common.interlay.dto;

import nl.jqno.equalsverifier.EqualsVerifier;
import nl.jqno.equalsverifier.Warning;
import org.junit.Test;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

import static org.junit.Assert.assertEquals;

public class M2mClientScopeDTOTest {

    @Test
    public void constructWithSettersTest(){
        M2mClientScopeDTO clientScopeDTO = new M2mClientScopeDTO();
        clientScopeDTO.setName("name");
        clientScopeDTO.setDescription("Description");
        clientScopeDTO.setProtocolMappers(Collections.singletonList(new ProtocolMapperDTO()));
        clientScopeDTO.setAttributes(Collections.singletonMap("key","value"));

        assertEquals("name",clientScopeDTO.getName());
        assertEquals("Description",clientScopeDTO.getDescription());
        assertEquals(Collections.singletonList(new ProtocolMapperDTO()),clientScopeDTO.getProtocolMappers());

        Map<String,String> attributes= new HashMap<>();
        attributes.put("include.in.token.scope","true");
        attributes.put("display.on.consent.screen","false");
        attributes.put("key","value");
        assertEquals(attributes,clientScopeDTO.getAttributes());
        assertEquals("openid-connect",clientScopeDTO.getProtocol());
    }

    @Test
    public void equalsTest(){
        EqualsVerifier.forClass(M2mClientScopeDTO.class)
                .usingGetClass()
                .suppress(Warning.NONFINAL_FIELDS)
                .verify();
    }

}