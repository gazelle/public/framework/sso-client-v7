package net.ihe.gazelle.ssov7.m2m.server.interlay.provider;


import net.ihe.gazelle.ssov7.m2m.server.utils.PublicKeyGenerator;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import javax.xml.bind.DatatypeConverter;
import java.io.File;
import java.security.KeyFactory;
import java.security.KeyPair;
import java.security.NoSuchAlgorithmException;
import java.security.PublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;

import static org.junit.Assert.assertEquals;

public class LocalPKProviderTest {

    private String publicKeyString;
    private final PublicKeyGenerator publicKeyGenerator = new PublicKeyGenerator();

    @Before
    public void setUp() {
        KeyPair keyPair = publicKeyGenerator.generateKeyPair();
        publicKeyString = publicKeyGenerator.getPublicKeyString(keyPair.getPublic());
        publicKeyGenerator.generatePublicKeyFileFromString("mockKey.pem", publicKeyString);
    }

    @Test
    public void getPublicKeyLocal() {
        LocalPKProvider ssoPublicKeyProvider = new LocalPKProvider("./mockKey.pem");
        assertEquals(formatPublicKey(publicKeyString), ssoPublicKeyProvider.getPublicKey());
    }

    @After
    public void tearDown() {
        //removing the generated public key
        File publicKeyFile = new File("./mockKey.pem");
        publicKeyFile.deleteOnExit();
    }

    private PublicKey formatPublicKey(String publicKeyString) {
        //removing all useless content to get only the key
        publicKeyString = publicKeyString
                .replace("-----BEGIN PUBLIC KEY-----", "")
                .replace("\r", "")
                .replaceAll(System.lineSeparator(), "")
                .replace("-----END PUBLIC KEY-----", "");
        byte[] publicKeyBytes = DatatypeConverter.parseBase64Binary(publicKeyString);

        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);
        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            return keyFactory.generatePublic(keySpec);
        } catch (NoSuchAlgorithmException | InvalidKeySpecException e) {
            throw new RuntimeException(e);
        }

    }

}