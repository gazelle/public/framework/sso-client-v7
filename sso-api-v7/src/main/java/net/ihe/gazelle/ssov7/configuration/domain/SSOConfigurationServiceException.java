package net.ihe.gazelle.ssov7.configuration.domain;

public class SSOConfigurationServiceException extends RuntimeException {

    public SSOConfigurationServiceException() {
    }

    public SSOConfigurationServiceException(String message) {
        super(message);
    }

    public SSOConfigurationServiceException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SSOConfigurationServiceException(Throwable throwable) {
        super(throwable);
    }
}
