package net.ihe.gazelle.ssov7.authn.domain;

import java.io.Serializable;

/**
 * Identity of a user interacting with the application. This component is supposed to be injected.
 */
public interface GazelleIdentity extends Serializable {

   /**
    * Is the user logged-in.
    *
    * @return true if the user is authenticated, false otherwise.
    */
   boolean isLoggedIn();

   /**
    * Get the username of the user. Only set if the user is logged-in.
    *
    * @return username of the user.
    */
   String getUsername();

   /**
    * Get the firstName of the user. Only set if the user is logged-in
    *
    * @return firstName of the user.
    */
   String getFirstName();

   /**
    * Get the lastName of the user. Only set if the user is logged-in
    *
    * @return lastName of the user.
    */
   String getLastName();

   /**
    * Get the email of the user. Only set if the user is logged-in
    *
    * @return email of the user.
    */
   String getEmail();

   /**
    * Get the keyword of the user's organisation. Only set if the user is logged-in
    *
    * @return keyword of the user's organisation.
    */
   String getOrganisationKeyword();

   /**
    * @return the name to be display in the UI. Usually a concatenation of the lastName and the firstName.
    */
   String getDisplayName();

   /**
    * Does the user has the requested role.
    *
    * @param role role to check.
    *
    * @return true if the user has this role, false otherwise.
    */
   boolean hasRole(String role);

   /**
    * Does the user has the permission to perform the given action on the given resource type and according to the given
    * context.
    *
    * @param name     name of the resource type.
    * @param action   action to be performed on that resource type.
    * @param contexts contextual objects that may be required by the permission resolver to compute the decision.
    *
    * @return true if the permission is granted, false otherwise.
    */
   boolean hasPermission(String name, String action, Object... contexts);

   /**
    * Does the user has the permission to perform the given action on the given resource.
    *
    * @param target resource targeted by the action.
    * @param action action to be performed.
    *
    * @return true if the permission is granted, false otherwise.
    */
   boolean hasPermission(Object target, String action);

}
