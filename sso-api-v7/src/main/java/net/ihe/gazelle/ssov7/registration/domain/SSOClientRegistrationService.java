package net.ihe.gazelle.ssov7.registration.domain;

/**
 * Component for registering a service as client to SSO for user authentication
 */
public interface SSOClientRegistrationService {

    /**
     * Register a service as a client to the SSO for enabling user authentication.
     *
     * @param clientId        the client id of the service to register.
     * @param applicationUrl  the public URL of the service to register.
     * @throws IllegalArgumentException if clientId or applicationUrl are null
     */
    void registerClient(String clientId, String applicationUrl);
}
