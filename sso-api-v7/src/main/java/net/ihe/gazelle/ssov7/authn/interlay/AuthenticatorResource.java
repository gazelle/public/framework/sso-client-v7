package net.ihe.gazelle.ssov7.authn.interlay;

import java.security.Principal;

public class AuthenticatorResource {
    private String email;
    private String firstName;
    private String lastName;
    private String organizationKeyword;
    private String username;
    private String roles;
    private Principal principal;
    private boolean loggedIn;
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrganizationKeyword() {
        return organizationKeyword;
    }

    public void setOrganizationKeyword(String organizationKeyword) {
        this.organizationKeyword = organizationKeyword;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoles() {
        return roles;
    }

    public void setRoles(String roles) {
        this.roles = roles;
    }

    public Principal getPrincipal() {
        return principal;
    }

    public void setPrincipal(Principal principal) {
        this.principal = principal;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }
}
