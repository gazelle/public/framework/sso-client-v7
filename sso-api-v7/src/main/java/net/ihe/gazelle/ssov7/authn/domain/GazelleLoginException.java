package net.ihe.gazelle.ssov7.authn.domain;

public class GazelleLoginException extends RuntimeException {
   private static final long serialVersionUID = -5538114255794594817L;

   public GazelleLoginException() {
   }

   public GazelleLoginException(String s) {
      super(s);
   }

   public GazelleLoginException(String s, Throwable throwable) {
      super(s, throwable);
   }

   public GazelleLoginException(Throwable throwable) {
      super(throwable);
   }
}
