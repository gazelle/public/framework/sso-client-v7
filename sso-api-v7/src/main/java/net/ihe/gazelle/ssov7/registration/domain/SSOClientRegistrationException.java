package net.ihe.gazelle.ssov7.registration.domain;

public class SSOClientRegistrationException extends RuntimeException {

    public SSOClientRegistrationException() {
    }

    public SSOClientRegistrationException(String message) {
        super(message);
    }

    public SSOClientRegistrationException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public SSOClientRegistrationException(Throwable e) {
        super(e);
    }
}
